\documentclass[9pt]{beamer}
%\documentclass[handout, 9pt]{beamer}

\usepackage{import}
\subimport{../}{preamble.tex}
\subimport{../}{beamer-theme.tex}

% Location for all figures / pictures
\graphicspath{{pictures/}{figures/}}

% Figures inline
\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{calc}
\usetikzlibrary{shapes.multipart}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{arrows.meta}
\usepgfplotslibrary{fillbetween}

\newcommand{\xbar}{1}


%--------------------------------------------------
% Presentation Information
%--------------------------------------------------
\title[15.084J/6.252J Nonlinear Optimization]{15.084J/6.252J : Introduction to Nonlinear Optimization}
\date[]{Spring 2020}
%--------------------------------------------------

\begin{document}

\begin{frame}  

  \maketitle

  \vspace{-2em}
  
  \begin{center}
    \begin{tabular}{lll}
      Instructor : & \textbf{Bart Van Parys} & \textbf{(\href{mailto:vanparys@mit.edu}{vanparys@mit.edu})} \\
      TA : & Chenyang Yuan & (\href{mailto:ycy@mit.edu}{ycy@mit.edu})
    \end{tabular}
  \end{center}
  
\end{frame}

\section{History}

\begin{frame}{Nature As Optimizer : Fermat's Principle}

  Fermat : ``Light takes the fastest optimal path (in hindsight)'' 
  
  \begin{center}
    \includegraphics[height=4cm]{snell.pdf}
  \end{center}

  \uncover<2->{
  Travel time:
  \[
    f(x) = \frac{\sqrt{x^2+a^2}}{c/n_1} + \frac{\sqrt{b^2 + (l-x)^2}}{c/n_2}
  \]

  \begin{itemize}
  \item The actual observed $x^\star$ will be a minimizer of $f(x)$
  \item Is equivalent to Snell's law: $n_1\sin(\theta_1)=n_2\sin(\theta_2)$
  \end{itemize}
  }
\end{frame}

\begin{frame}{History of Optimization -- Univariate Problems}

  \begin{center}
    \includegraphics[height=4cm]{univariate-function/univariate-function.pdf}
  \end{center}
  
  \begin{itemize}
  \item Fermat, 1638 and Newton, 1670
    $$ x^\star \in \arg \min \; f(x)\qquad \mbox{$x$: scalar} $$

    Necessary condition : $$ f'(x^\star)=0 $$

  \end{itemize}
\end{frame}

\begin{frame}{History of Optimization -- Multivariate Problems}

  \begin{center}
    \includegraphics[height=4cm]{rosenbrock/rosenbrock.pdf}
  \end{center}
  
  \begin{itemize}
  \item Euler, 1755 
    $$  \min \; f(x_1, \ldots, x_n)  $$
    Necessary condition : $$ \nabla f({\bf{x}})=\bold{0}   $$  
  \end{itemize}
\end{frame}


\begin{frame}{History of Optimization -- Constraint Problems}

  Constraint mechanics
  \begin{center}
    \includegraphics[height=2.5cm]{lagrangian-mechanics.pdf}
  \end{center}

  \begin{itemize}
  \item Lagrange, 1797: ``M\'ecanique Analytique''

    \[
      \begin{array}{rl}
        \min & f(x_1, \ldots, x_n)\\[0.5em]
        \st & \forall k=1,\ldots,m:\\
            & \quad  g_k(x_1, \ldots, x_n)=0.
      \end{array}
    \]
  \item Method of Lagrange multipliers (See duality theory!)
  \end{itemize}
\end{frame}

\begin{frame}{History of Optimization -- Calculus of Variations}

  Which slide is the fastest? (Johan Bernouilli 1696)
  \begin{center}
    \includegraphics[height=3cm]{brachistochrone_curve.png}
  \end{center}
  \begin{itemize}
  \item Brachistochrone (``Shortest time'')
  \end{itemize}
  
    \hfill
  \begin{itemize}
  \item Euler, Lagrange : Calculus of variations. 
  \end{itemize}

\end{frame}

\begin{frame}{Huygens's Optimal Pendulum}

  Is there a pendulum which frequency is independent of its amplitude?
  \begin{center}
    \includegraphics[height=5cm]{tautochrone_curve.jpg}
  \end{center}
  \begin{itemize}
  \item Tautochrone (``Equal time'')
  \end{itemize}
  
    \hfill
  \begin{itemize}
  \item<2-> Huygens' implementation was a failure\dots
  \end{itemize}

\end{frame}


\section{Least Squares Optimization}

\begin{frame}{Least Squares Regression}

  Supervised Data
  \begin{itemize}
  \item \makebox[3.225cm]{Covariates : \hfill} $X \defn (x_1, \dots, x_n) \in \Re^{n\times p}$
  \item \makebox[3.225cm]{Labels : \hfill} $Y \defn (y_1, \dots, y_n) \in \Re^n$
  \end{itemize}
  \vspace{1em}
  \makebox[3.9cm]{\hfill} $Y \approx X \mathbf{\blue{w}}$
  \vspace{0.5em}
  
  \textbf{Least Squares Regression :}
  \vfill
  \begin{equation*}
    \begin{array}{rl}
      w^\star \in \arg \min_{w\in\Re^p} & \sum_{i=1}^n (y_i - x_i\tpose w)^2
    \end{array}
  \end{equation*}
  \vfill

  
  \begin{minipage}{0.65\textwidth}
    \textit{Math history :}
    \begin{itemize}
    \item<2-> \textit{Legendre (1805) : Least-squares method}\\
    \item<3-> \textit{Gau\ss~(1809) : Celestial mechanics of Ceres}\\
    \item<3-> Big data in 1809: $n=22$!
    \item<3-> Gauss only used $n=3$ observations\dots
    \end{itemize}
  \end{minipage}%
  \hfill
  \begin{minipage}{0.35\textwidth}
    \uncover<2->{%
    \begin{minipage}{0.5\textwidth}
      \centering
      \includegraphics[height=2.5cm]{young-legendre.jpg}\\
      Legendre
    \end{minipage}%
    }%
    \hfill
    \uncover<3->{%
    \begin{minipage}{0.5\textwidth}
      \centering
      \includegraphics[height=2.5cm]{young-gauss.jpg}\\
      Gau\ss
    \end{minipage}
    }%
  \end{minipage}

  \vfill

\end{frame}

\begin{frame}{Least Squares Optimization}

  \begin{minipage}{1\textwidth}
    Least Squares Matrix Notation :
    \vfill
    \begin{equation*}
      \begin{array}{rl}
        \minimize & \norm{Y - X w}^2_2\\[0.5em]
        \st & w\in\Re^p
      \end{array}
    \end{equation*}
  \end{minipage}%
  % \hfill
  % \begin{minipage}{0.4\textwidth}
  %   \centering
  %   \includegraphics[height=3.5cm]{least-squares/least-squares.pdf}
  % \end{minipage}
  \vfill

  \textbf{Quadratic Optimization Problem :}
  \begin{itemize}
  \item Analytical solution as a linear equation
    \[
      (X\tpose X)\,w^\star = X\tpose Y
    \]

  \item Reliable and efficient algorithms and software
  \item Computational time scales as $n^2 p$ ($X\in\Re^{n\times p})$
  \item<2-> Mature technology
  \end{itemize}
  
\end{frame}

\section{Linear Optimization}

\begin{frame}{Linear Optimization}

  \begin{minipage}{0.5\textwidth}
    \textbf{Linear Optimization :}\\[0.5em]
    \begin{equation*}
      \begin{array}{rl}
        \minimize & c\tpose x \\[0.5em]        
        \st & x\in\Re^n \\[0.5em]
        & a_i\tpose x \leq b_i, \quad \forall i \in [1, \dots, k]
      \end{array}
    \end{equation*}
  \end{minipage}%
  \begin{minipage}{0.5\textwidth}
    \centering
    \includegraphics[height=3.5cm]{linear-optimization/linear-optimization.pdf}
  \end{minipage}

  \vfill
  
  \begin{minipage}{0.6\textwidth}    
    \textbf{Properties :}
    \begin{itemize}
    \item No analytical solution
    \item Reliable and efficient algorithms and software
    \item Computational time scales as $n^2 k$
    \item<2-> Mature technology
    \end{itemize}
  \end{minipage}%
  \hfill
  \begin{minipage}{0.2\textwidth}
    \includegraphics[height=2.5cm]{kantorovich.jpg}\\
    Kantorovich
  \end{minipage}%
  \begin{minipage}{0.2\textwidth}
    \includegraphics[height=2.5cm]{dantzig.jpg}\\
    Dantzig
  \end{minipage}

\end{frame}

\begin{frame}{Transport Logistics}

  We have $n$ consumers requiring a \emph{demand} $d_1, \dots, d_n$ of product. Every consumer needs to be serviced by any of $m$ distribution facilities stocking a \emph{supply} $s_1, \dots, s_m$. The \emph{marginal cost} of shipping from supply facility $j$ to consumer $i$ is denoted as $c_{ij}$.


  \begin{center}
    \includegraphics[height=3.5cm]{transport/transport.pdf}
  \end{center}
  
  Define : $x_{ij}$ as the \emph{amount supplied} from the $j$-th facility to the $i$-th consumer.
  
  \vfill

  \uncover<2->{
  \[
    \begin{array}{rl}
      \minimize & \sum_{i=1}^n \sum_{j=1}^m c_{ij} x_{ij} \\[0.5em]
      \st & x_{ij} \geq 0 \quad i\in [1, \dots, n], ~j\in [1, \dots, m] \\[0.5em]
                & \sum_{i=1}^n x_{ij} \leq s_j\quad j\in [1, \dots, m] \\[0.5em]
                & \sum_{j=1}^m x_{ij} \geq d_i\quad i\in [1, \dots, n]
    \end{array}
  \]
  }
  
\end{frame}

\begin{frame}{Statistical Risk -- Problem}

  \emph{Example} : Consider revenue $R$ is distributed on $n$ equidistant points $r_i$ on the interval $\Omega = [-10, 100]$.
  We assume the following prior information
  \begin{itemize}
  \item Mean : $\E{}{R} \in [15, 20]$
  \item Second-order moment : $\E{}{R^2} \in [500,600]$
  \item Higher-order information : $\E{}{3 R^3-2 R} =40000$ 
  \end{itemize}

  \vfill

  \emph{Problem:} What is the worst probability of loss $\mb P(R<0)$?

\end{frame}

\begin{frame}{Statistical Risk -- Solution}
  
  Decision variable : $p_i \defn \mb P(R=r_i)$.
  
  \vfill
  
  \begin{equation*}
    \begin{array}{rl}
      {\displaystyle\max_{p}} & \sum_{\set{i\in[1, \dots, n]}{r_i < 0}} p_i \\[0.5em]
      \st & p\geq 0, ~\sum_{i=1}^n p_i = 1,\\[0.5em]
           &  15 \leq \sum_{i=1}^n r_i p_i \leq 20,\\[0.5em]
                & 500 \leq \sum_{i=1}^n r^2_i p_i \leq 600,\\[0.5em]
                & \sum_{i=1}^n \left( 3 r^3_i - 2r_i\right) p_i = 40000.
    \end{array}
  \end{equation*}

  \vfill

  \begin{itemize}
  \item Solution : see provided Julia program.
  \end{itemize}
  
\end{frame}

\section{Nonlinear Optimization}

\begin{frame}{Optimization}
  Optimization is about making good \emph{decisions} in a \emph{disciplined} way, often subject to constraints. Applications appear everywhere in science, mathematics and business:

  \vfill
  
  \begin{itemize}
  \item Managing a portfolio
  \item Scheduling public transport
  \item Fitting a model to measured data
  \item Optimizing a supply chain
  \item Designing electronic circuits
  \item Choosing worker shift patterns
  \item Shaping aerodynamic components
  \item Recovering images from ray MRI data
  \end{itemize}

  \vfill

  Please leave some suggested application areas on Canvas!

\end{frame}


\begin{frame}{Describing an Optimization Problem}

  \textbf{Optimization Problem :}

  \vfill
  \[
    \begin{array}{rl}
      \minimize & f(x) \\[0.5em]
      \st & x \in E, \\[0.5em]
                & x\in X \subseteq E.
    \end{array}
  \]

  \vfill

  An optimization problem of several parts
  \begin{itemize}
  \item The vector $x$ collects the \emph{optimization variables}
  \item The linear space $E(=\Re^n, \Re^{n\times n})$ is the \emph{domain} of the decision variables
  \item The set $X$ is the \emph{constraint set}, and describes the \emph{feasible decisions}
  \item The \emph{objective} function $f:E\to\Re$ assigns cost $f(x)$
  \end{itemize}

\end{frame}

\begin{frame}{Terminology}

  \emph{Solving} the \emph{Nonlinear Optimization} problem :
  \[
    \begin{array}{rl}
      \minimize & f(x) \\[0.5em]
      \st & x \in E, \\[0.5em]
                & x\in X \subseteq E,
    \end{array}
  \]
  requires determining
  \begin{itemize}
  \item the infimum (greatest lower bound)
    \[
      \begin{array}{rl}
        J^\star = \inf_x & f(x) \\
        \st & x\in X \subseteq E
      \end{array}
    \]
  \item the set of minimizers
    \(
      \arg \min_{x\in X} f(x) \defn \set{x\in X\subseteq E}{f(x)\leq J^\star\red{+\epsilon}}
    \)
  \end{itemize}
  \vfill
  
  Terminology :
  \begin{itemize}
  \item<2-> If $J^\star = -\infty$, then the problem is \emph{unbounded below}
  \item<3-> If the set $X$ is empty, then the problem is \emph{infeasible} ($J^\star=\infty$)
  \item<4-> If the set $X = E$, then the problem is \emph{unconstrained}
  \end{itemize}
  
\end{frame}


\begin{frame}{Nonlinear Optimization Problem}

  \textbf{More common problem formulation :}

  \vfill
  \[
    \begin{array}{rl}
      \minimize & f(x) \\[0.5em]
      \st    & {x\in E} \\[0.5em]
             & g_i(x) \leq 0 \quad \forall i \in [1, \dots, m]\\[0.5em]
             & h_i(x) = 0 \quad \forall i \in [1, \dots, \ell]
    \end{array}
  \]
  \vfill
  
  Defined by the problem data :
  \begin{itemize}
  \item \emph{Objective function} $f:E\to\Re$
  \item \emph{Domain} $E$ of the objective function 
  \item \emph{Inequality constraint functions} $g_i : E\to\Re$ for $i$ in $[1, \dots, m]$
  \item \emph{Equality constraint functions} $h_i : E\to\Re$ for $i$ in $[1,\dots, \ell]$
  \item \emph{Feasible set}
    \[
      X = \set{x\in E}{g_i(x) \leq 0 ~~\forall i \in [1, \dots, m],~h_i(x) = 0 ~~ \forall i \in [1, \dots, \ell] }
    \]
  \end{itemize}
  
  
\end{frame}

\begin{frame}{Nonlinear Optimization Problem : Example}

  \begin{minipage}{0.55\textwidth}

    \textbf{Problem :} In $\Re^2$, find the point in the unit box $X$ closest to the point $(x_1, x_2) = (3, 2)$.

    \vspace{3cm}
    
  \end{minipage}%
  \hfill
  \begin{minipage}{0.45\textwidth}
    \centering
    \includegraphics[height=4cm]{constraint-optimization/constraint-optimization.pdf}
  \end{minipage}

  \vfill
  
  \textbf{Standard Form}

  \begin{equation*}
    \begin{array}{rl}
      \minimize & (x_1-3)^2+(x_2-2)^2 \\[0.5em]
      \st & (x_1, x_2) \in \Re^2, \\[0.5em]
                & x_1 \leq 1, ~-x_1 \leq 1,\\[0.5em]
                & x_2 \leq 1, ~-x_2 \leq 1.
    \end{array}
  \end{equation*}
  
\end{frame}

\begin{frame}{Terminology}

  Nonlinear optimization problem :
  \vfill
  \[
  \begin{array}{rl}
    J^\star = \inf & f(x) \\[0.5em]
    \st    & x\in E\\[0.5em]
           & g_i(x) \leq 0, \quad \forall i \in [1, \dots, m]\\[0.5em]
                     & h_i(x) = 0, \quad \forall i \in [1, \dots, \ell]
  \end{array}
  \]
  \vfill
  
  \begin{itemize}
  \item \emph{Feasible point} : A point $x$ in $E$ satisfying the inequality and equality constraints, i.e., $g_i(x)\leq 0$ for $i$ in $[1, \dots, m]$ and $h_i(x)=0$ for $i$ in $[1, \dots, \ell]$.

  \item \emph{Strictly feasible point} : A feasible point $x$ in $E$ satisfying the inequality constraints strictly, i.e., $g_i(x)<0$ for $i$ in $[1, \dots, m]$.

  \item \emph{Minimum} : The minimum $J^\star$

  \item A \emph{redundant constraint} does not influence the feasible set.

    \[
      \begin{array}{rl}
        \min_{x\in\Re} & f(x) \\
        \st & x\leq 1 \\
                       & x\leq 3 \quad \emph{redundant}
      \end{array}
    \]
  \end{itemize}
  
\end{frame}

\begin{frame}{Solution Types}

  \[
    \begin{array}{rl}
      \minimize & f(x) \\[0.5em]
      \st & x\in E,\\[0.5em]
          & x\in X \subseteq E.
    \end{array}
  \]

  \vfill
    
  Solution Types:
  \begin{itemize}
  \item \emph{Global minimizer} : Any feasible $x^\star$ in $X$ such that $f(x^\star) \leq f(x)$ for all feasible $x\in X$.
    \vspace{0.5em}
  \item \emph{Local minimizer} : Any feasible $x^\star$ in $X$ such that $f(x^\star) \leq f(x)$ for all $x\in X \cap B(x^\star, \epsilon)$ for some ball $B(x^\star, \epsilon) \defn \set{x\in E}{\norm{x-x^\star}<\epsilon}$.
  \end{itemize}
  
\end{frame}

\begin{frame}{Potential Problems}

  \begin{enumerate}
  \item If the constraints are inconsistent, then the problem is \emph{infeasible}.
    \[
      \begin{array}{rl}
        \minimize & \exp{(x)} \\
        \st  & x\leq -1 \\
                  & x \geq 0
      \end{array}
    \]

  \item It might be possible that the problem is \emph{unbounded}.
    \[
      \begin{array}{rl}
        \minimize & x \\
       \st   & x\leq 0 \\
      \end{array}
    \]

  \item The value $J^\star$ might be finite, but no optimizer exists.
    \[
      \begin{array}{rl}
        \minimize & \exp{(x)}
      \end{array}
    \]
    The infimum is $J^\star=0$, but no optimal solution achieves it.
  \end{enumerate}
  
\end{frame}

\begin{frame}{Geometry of an Optimization Problem}

  \centering
  \includegraphics[width=0.65\textwidth]{geometry.png}

\end{frame}

\section{Convex Optimization}

\begin{frame}{Convex Optimization}


  \begin{minipage}{0.55\textwidth}
    \textbf{Important Special Case :}
    \[
      \begin{array}{rl}
        \minimize & f(x) \\[0.5em]
        \st    & x\in E\\[0.5em]
                  & g_i(x) \leq 0, \quad \forall i \in [1, \dots, m],
      \end{array}
    \]
    
    where the functions objective and constraint functions are \emph{convex functions}. 
  \end{minipage}%
  \hfill
  \begin{minipage}{0.45\textwidth}
    
    \begin{center}
      \includegraphics[height=4.5cm]{convex-function/convex-function.pdf}
    \end{center}
    
  \end{minipage}

  \vfill
  A function $f$ is convex when
  \[
    \forall x, y\in E, ~\forall \theta \in [0, 1] : \quad f(\theta\cdot x+(1-\theta)\cdot y) \leq \theta\cdot f(x) + (1-\theta)\cdot f(y).
  \]
  \vfill

  \begin{itemize}  
  \item Includes least-squares and linear optimization as special cases
  \end{itemize}

\end{frame}

\begin{frame}{Brief History of Convex Optimization}

  \textbf{Theory (convex analysis)} : ca 1900--1970
  \vfill
  \textbf{Applications}
  \begin{itemize}
  \item before 1990: mostly in operations research; few in engineering
  \item since 1990: many new applications in engineering (control, signal processing, communications, \dots); new problem classes (semi-definite optimization, robust optimization)
  \end{itemize}
  \vfill
  \textbf{Algorithms}
  \begin{itemize}
  \item 1947: Simplex algorithm for linear programming (Dantzig)
  \item 1960s: Early interior-point methods (Fiacco \& McCormick, Dikin, \dots)
  \item 1970s: Ellipsoid method and other subgradient methods
  \item 1980s: Polynomial-time interior-point methods for linear programming (Karmarkar)
  \item late 1980s--now: Polynomial-time interior-point methods for nonlinear convex optimization (Nesterov \& Nemirovski)
  \end{itemize}
  
\end{frame}


\begin{frame}{Course Overview}

  \textbf{Introduction}
  \begin{enumerate}
  \item Convex Sets
  \item Convex functions
  \item Modeling
  \end{enumerate}

  \vfill
  
  \textbf{Theory}
  \begin{enumerate}
  \item Duality
  \item Conjugate Functions
  \item KKT
  \end{enumerate}

  \vfill

  \textbf{Algorithms}
  \begin{enumerate}
  \item Black Box Methods
  \item Gradient Method
  \item Newton Method
  \item Interior Point Methods
  \item ADMM, SGD
  \end{enumerate}

  \vfill
  
  \textbf{Applications?}
  
  
\end{frame}


\begin{frame}{SpaceX Falcon Heavy}

  \begin{minipage}{0.5\textwidth}
    \includegraphics[height=3cm]{spacex.jpg}
  \end{minipage}

  \vfill
  
  Land booster rockets back on earth via \emph{powered descent}.
  
\end{frame}

\begin{frame}{Powered Descent}

  \begin{minipage}{1\textwidth}
    \centering
    \includegraphics[height=2.5cm]{landing.jpg}
  \end{minipage}

  \vfill

  \emph{Minimum fuel guidance} problem is formulated and solved as a convex optimization problem!

  \vfill
  
  \begin{minipage}{1\textwidth}
    \centering
    \includegraphics[height=3cm]{powered-descent.jpg}
  \end{minipage}
  
\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
