\documentclass[9pt]{beamer}
% \documentclass[handout, 9pt]{beamer}

\usepackage{import}
\subimport{../}{preamble.tex}
\subimport{../}{beamer-theme.tex}

% Location for all figures / pictures
\graphicspath{{pictures/}{figures/}}

% Figures inline
\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{calc}
\usetikzlibrary{shapes.multipart}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{arrows.meta}
\usepgfplotslibrary{fillbetween}

\newcommand{\xbar}{1}

%--------------------------------------------------
% Presentation Information
%--------------------------------------------------
\title[15.084J/6.252J Nonlinear Optimization]{15.084J/6.252J : Convex Sets, Cones and Generalized Inequalities }
\date[]{Spring 2020}
%--------------------------------------------------

\begin{document}

\begin{frame}  

  \maketitle

  \vspace{-2em}
  
    \begin{center}
    \begin{tabular}{lll}
      Instructor : & \textbf{Bart Van Parys} & \textbf{(\href{mailto:vanparys@mit.edu}{vanparys@mit.edu})} \\
      TA : & Chenyang Yuan & (\href{mailto:ycy@mit.edu}{ycy@mit.edu})
    \end{tabular}
  \end{center}
  
\end{frame}

\begin{frame}{Previous Lecture}

  \begin{itemize}
  \item Historical context
  \item Optimization terminology
  \item<2-> Convex optimization as generalized linear optimization
    \[
      \begin{array}{rl}
        \minimize & f(x) \\[0.5em]
        \st & x \in C
      \end{array}
    \]
    with
    
    \vfill
    
    \begin{enumerate}
      \setlength\itemsep{1em}
    \item Feasible region $C$ a \emph{convex set}
    \item Objective function $f$ a \emph{convex function}
    \end{enumerate}
  \end{itemize}



\end{frame}

\section{Convex Sets}

\begin{frame}{Linear Sets}

  A set $L \subseteq E$ is \emph{linear} if $x, y \in L$ implies
  \[
    a x + b y \in L,
  \]
  for any $a, b \in \Re$.

  \vfill
  \begin{center}
    \includegraphics[height=3cm]{linear-subspace.pdf}
  \end{center}
  \vfill

  \textbf{Examples :}
  \begin{itemize}
  \item Linear spaces such as $\Re^n$, $\Re^{n\times m}$
  \item Symmetric matrices $S^n\defn \set{X\in\Re^{n\times n}}{X=X\tpose}$
  \item Null space $\set{x\in\Re^m}{Ax=0}$ of a matrix $A\in\Re^{n\times m}$ 
  \end{itemize}
  
\end{frame}

\begin{frame}{Linear Hull}

  The \emph{linear hull} $span (S) \defn \cap_{L~\rm{linear},\, S\subseteq L} ~L$ of a set $S$ is the unique smallest linear set that contains $L$, i.e.\
  $$ L~\mathrm{linear~set}, ~S \subseteq L \implies span(S) \subseteq L$$  

  \vfill

  The \emph{dimension} $\dim(L)=k$ of a linear space $L$ is the smallest number of vectors $v_i\in L$ such that $$span(\{v_1, \dots, v_k\}) = L.$$

  \vfill
  
  Example:
  \begin{itemize}
  \item Matrices : $\dim(\Re^{n\times n})=n^2$
  \item Symmetric matrices: \uncover<2->{$\dim(S^n) = (n+1)n/2$}
  \end{itemize}
  
\end{frame}

\begin{frame}{Affine Sets}

    
  A set $H \subseteq E$ is \emph{affine} if $x, y \in H$, $\theta \in \Re$ implies
  \[
    \theta x + (1-\theta) y \in H.
  \]    

  \vfill
  \begin{center}
    \includegraphics[height=1.5cm]{affine-set/affine-set.pdf}
  \end{center}
  \vfill

  
  \textbf{Examples :}

  \begin{itemize}
  \item Linear spaces are affine
  \item Solution set of linear system $\set{x\in\Re^m}{Ax = b}$ with $A\in \Re^{m\times n}$
  \end{itemize}

  \vfill

  \uncover<2->{
  If $H$ is an affine set, then $\set{x-x_0}{x\in H}$ with $x_0\in H$ is a \emph{linear subspace}.\\[0.5em]
  The \emph{dimension} $\dim(H)$ of an affine set is the dimension of its associated subspace.
  }
\end{frame}

\begin{frame}{Affine Hull}

  The \emph{affine hull} $\aff (S) \defn \cap_{H~\rm{affine},\, S\subseteq H} ~H$ of a set $S$ is the unique smallest affine set that contains $S$, i.e.\
  $$ H~\mathrm{affine~set}, ~S \subseteq H \implies \aff(S) \subseteq H$$  

  \vfill
  \begin{minipage}{1\textwidth}
    \begin{center}
      \includegraphics[height=4cm]{affine-hull/affine-hull.pdf}
    \end{center}
  \end{minipage}
  \vfill
  
  \uncover<2->{The \emph{affine dimension} $\dim(S)$ of a set $S$ is the dimension of $\aff(S)$ (nonstandard)}

\end{frame}

\begin{frame}{Affine Combinations}

  Consider a finite set of points $x_i$ for $i$ in $I=[1, \dots, k]$. An \emph{affine combination} is any point of the form
  \[
    \sum_{i\in I} p_i \cdot x_i
  \]
  where $p_i\in\Re$ and $\sum_{i\in I} p_i=1$.

  \vfill
  
  \textbf{Theorem : } An affine set $H \subseteq E$ is closed under affine combinations.

  \vfill
  
  \textbf{Theorem : } The affine hull of a set $S$ can be found as
  \[
    \aff (S) = \set{\sum_{i\in I} p_i\cdot x_i}{\forall k\in \N, ~\forall i\in I=[1, \dots, k], ~x_i \in S, ~\sum_{i\in I} p_i=1}.
  \]
  
\end{frame}

\begin{frame}{Relative Interior}

  If the set $S$ is a \emph{lower dimensional subset} ($\dim(S) < \dim(E)$) of a space $E$, then $\interior S=\emptyset.$ The set $S$ is degenerate in the topology of the space $E$. 

  \vfill

  The define the \emph{relative interior} $\relint(S)$ of a set $S$ as
  \[
    \relint(S) \defn \set{x\in S}{\exists \epsilon>0:~B(x, \epsilon) \cap \aff(S) \subseteq S}.
  \]

  \vfill
  
  \begin{minipage}{1\textwidth}
    \begin{center}
      \includegraphics[height=3.5cm]{affine-hull/affine-hull.pdf}
    \end{center}
  \end{minipage}

\end{frame}

\begin{frame}{Convex Sets}
  
  A set $C \subseteq E$ is \emph{convex} if $x, y \in C$ implies
  \[
    \theta x + (1-\theta) y \in C
  \]
  for all $\theta\in[0,1]$.
  \vfill
  
  \begin{minipage}{0.5\textwidth}
    \textbf{Example:}\\
    \vspace{-6mm}
    \begin{center}
      \includegraphics[height=3cm]{convex-sets/convex-sets-I.pdf}
    \end{center}
  \end{minipage}%
  \hfill
  \begin{minipage}{0.5\textwidth}
    \textbf{Counter Example:}\\
    \vspace{-6mm}
    \begin{center}
      \includegraphics[height=3cm]{convex-sets/convex-sets-III.pdf}
    \end{center}
  \end{minipage}

\end{frame}

\begin{frame}{Convex Hull}

  The \emph{convex hull} $\conv (S) = \cap_{C~{\rm{convex}},\, S\subseteq C} ~C$ of a set $S$ is the unique smallest convex set that contains $S$, i.e.\
  $$ C~\mathrm{convex~set}, ~S \subseteq C \implies \conv(S) \subseteq C$$  


  \vfill
  \textbf{Example:}
  The convex hull of a 15 arbitrary points
  \vfill
  \begin{center}
    \includegraphics[height=3cm]{convex-hull/convex-hull.pdf}
  \end{center}
  \vfill
  
\end{frame}

\begin{frame}{Convex Combinations}

  Consider a finite set of points $x_i$ for $i$ in $I=[1, \dots, k]$. A \emph{convex combination} of points $x_i$ with $i\in I$ is any point of the form
  \[
    \sum_{i\in I} p_i \cdot x_i,
  \]
  where $p_i \geq 0$ for all $i\in I$ and $\sum_{i\in I} p_i=1$.

  \vfill
  
  \textbf{Theorem : } A convex set $C \subseteq E$ is closed under convex combinations.

  \vfill
  
  \textbf{Theorem : } The convex hull of a set $S$ can be found as
  \[
    \conv (S) = \set{\sum_{i\in I} p_i\cdot x_i}{\forall k\in \N, ~\forall i\in I=[1, \dots, k], ~x_i \in S, ~p_i\geq 0, ~\sum_{i\in I} p_i=1}.
  \]
\end{frame}

\begin{frame}{Extreme Points}

  A point $x$ is called an \emph{extreme point} of a convex set $C$ if it can not be written as a strict convex combination of two distinct points in that set, i.e.\
  \[
    x \in \ex(C) \iff \,!\exists y\neq z \in C: ~~x = \theta y + (1-\theta) z
  \]
  with $\theta \in (0, 1)$.

  \vfill

  \textbf{Examples: }

  \vfill

  \begin{minipage}{0.5\textwidth}
    \centering
    \includegraphics[height=2.5cm]{extreme-points/extreme-points.pdf}
  \end{minipage}%
  \hfill
  \begin{minipage}{0.5\textwidth}
    \centering
    \includegraphics[height=2.5cm]{extreme-points/extreme-points-2.pdf}
  \end{minipage}
  
\end{frame}

\begin{frame}{Extreme Points}

  \textbf{Minkowski-Carath\'eodory} Let $A$ be a compact (closed and bounded) convex set. Then,
  \[
    A = \conv(\ex(A)).
  \]

  \vfill

  \textbf{Graphically:}
  \begin{center}
    \includegraphics[height=2.5cm]{minkowski-caratheodory.pdf}\\
    {\tiny (Source: Wikipedia)}
  \end{center}
  a compact convex set can be recovered from its extreme points.

  \vfill

  \uncover<2->{
  \textbf{Remarks:}
  \begin{itemize}
  \item It is important that the set is closed and bounded!
  \end{itemize}
  }
  
\end{frame}

\begin{frame}{Vertices}

  A point $x\in E$ is called a \emph{vertex} of a convex set $C$ if it can be written as the {unique} minimizer of
  \[
    \begin{array}{rl}
      \min & {c}\tpose {x} \\[0.5em]
      \st  &  x\in C
    \end{array}
  \]
  for some cost vector $c$.

  \vfill

  \textbf{Theorem:} A vertex is always an extreme point.

  \uncover<2->{
  \textbf{Proof [$\star$]:}

  Consider a vertex $v\in C$ with $\{v\} = \arg\min_{x\in C} c\tpose x$ for some cost vector $c$. Assume, for contradiction that $v$ is not an extreme point. That is, there exists $x_1\neq x_2\in C$ and $\theta\in (0, 1)$ so that
  \(
  v = \theta x_1+(1-\theta) x_2.
  \)
  We have $\max \{c\tpose x_1, c\tpose x_2\}>c\tpose v$ as $\{v\} = \arg\min_{x\in C} c\tpose x$ (unique minimizer). We also have $c\tpose v = \theta c\tpose x_1 + (1-\theta) c\tpose x_2$. As now $\theta\in (0, 1)$, this implies that $\min \{c\tpose x_1, c\tpose x_2\}<c\tpose v$; a contradiction with the optimality of $v$.
  }

  \vfill

  \uncover<3->{
    \textbf{Warning:} the reverse is not always true!
    (It is true for polyhedra)
    }
\end{frame}

\begin{frame}{Vertices $\subset$ Extreme Points}

  \textbf{Counter Example:}
  \[
    \set{(x_1, x_2)}{-1\leq x_1\leq 1, ~-2 \leq x_2 \leq 0} \cup \tset{(x_1, x_2)}{x_1^2+x_2^2\leq 1}
  \]

  \begin{center}
    \includegraphics[width=0.3\textwidth]{exposed-extreme-points/exposed.pdf}
  \end{center}

  \uncover<2->{
  The points $(-1, 0)$ and  $(1, 0)$ are extreme points but not a vertices!
  }
\end{frame}


\begin{frame}{Convex Cones}

  A set $K \subseteq E$ is \emph{conic} if $x \in K$ implies
  \[
    \theta x \in K
  \]
  for all $\theta\in\Re_+$. Cones are \textbf{not} necessarily convex sets.

  \vfill
  
  \begin{minipage}{0.5\textwidth}
    \begin{center}
      \centering
      \includegraphics[height=3cm]{convex-sets/convex-sets-II.pdf}
    \end{center}
  \end{minipage}%
  \hfill
  \begin{minipage}{0.5\textwidth}
    \begin{center}
      \centering
      \includegraphics[height=3cm]{convex-sets/convex-sets-IV.pdf}
    \end{center}
  \end{minipage}

  \vfill

  A set $K$ is a \emph{convex cone} if it is a cone and a convex set.
\end{frame}

\begin{frame}{Conic Hull}

  The \emph{conic hull} $\cone(S) \defn \cap_{K~\mathrm{convex~cone},\, S\subseteq K} K$ of a set $S$ is the unique smallest \textbf{convex} conic set that contains $S$, i.e.\
  $$ K~\mathrm{convex~cone}, ~S \subseteq K \implies \cone(S) \subseteq K$$  
  
  \vfill

  \textbf{Example:}
  The conic hull of 15 arbitrary points
  \begin{center}
    \includegraphics[height=3cm]{conic-hull/conic-hull.pdf}
  \end{center}

\end{frame}

\begin{frame}{Conic Combinations}

  Consider a finite set of points $x_i$ for $i$ in $I=[1, \dots, k]$. \emph{Conic combination} of points $x_i$ with $i\in I$, any point of the form
  \[
    \sum_{i\in I} p_i \cdot x_i,
  \]
  where $p_i \geq 0$ for all $i\in I$.

  \vfill
  
  \textbf{Theorem : } A cone $K \subseteq E$ is closed under conic combinations.

  \vfill
  
  \textbf{Theorem : } The conic hull of a finite dimensional set $S$ can be found as
  \[
    \cone (S) = \set{\sum_{i\in I} p_i\cdot x_i}{\forall k\in \N, ~\forall i\in I=[1, \dots, k], ~x_i \in S, ~p_i\geq 0}.
  \]


\end{frame}

\section{Standard Convex Sets}

\begin{frame}{Elementary Sets}

  Examples
  \begin{itemize}
  \item $\emptyset$ the empty set is affine (and thus convex)
  \item Singleton $\{x_0\}$ is affine
  \item A line is affine 
  \item Any open and closed norm ball,  $B(x, \epsilon)$ or $B[x, \epsilon]$, is convex
  \item Positive orthant $\Re^n_+ = \set{x\in \Re^n}{x_1\geq 0, \dots, x_n\geq 0}$ is a convex cone
  \end{itemize}
  
\end{frame}

\begin{frame}{Halfspace}

  \emph{Hyperplane} $\set{x\in E}{a\tpose x = b}$ with \emph{normal vector} $a\neq 0$

 
  \vfill
  \emph{Halfspace} $\set{x\in E}{a\tpose x \leq b}$ with normal vector $a\neq 0$

  \vfill

  \textbf{Some Properties :}
  \begin{itemize}
  \item Hyperplane is affine
  \item Halfspace is convex
  \item Halfspace with $b=0$ is a convex cone
  \end{itemize}
  
\end{frame}

\begin{frame}{Polyhedra}

  \begin{minipage}{0.5\textwidth}
    A \emph{polyhedral} set
    \[
      C = \set{x} {a_i\tpose x \leq b_i, \quad i \in [1, \dots, k]}
    \]
    is convex.
  \end{minipage}%
  \hfill
  \begin{minipage}{0.5\textwidth}
    \centering
    \includegraphics[height=3.5cm]{polyhedron/polyhedron.pdf}
  \end{minipage}

  \textbf{Special Cases:}
  \begin{itemize}
  \item The nonnegative orthant $\Re^n$
  \item The probability simplex
    \[
      \mc P_k = \set{p\in \Re^k}{\sum_{i=1}^k p_i=1, ~p_i\geq 0 ~~\forall i\in [1, \dots,k] }
    \]
  \item The convex hull of a finite set $\{x_1, \dots, x_k\}$
  \end{itemize}
  
\end{frame}

\begin{frame}{Ellipsoid}

  \emph{Euclidean ball} with center $x_c$ and radius r:
  \begin{align*}
    & \set{x}{\norm{x-x_c}_2^2 = (x-x_c)\tpose \frac{1}{r^2} (x-x_c) \leq 1} \\
    = & \set{x_c + r u}{\norm{u}_2 \leq 1}
  \end{align*}
  \vfill
  \emph{Ellipsoid} :
  \begin{align*}
    & \set{x}{\norm{x-x_c}_P^2 = (x-x_c)\tpose P^{-1} (x-x_c) \leq 1} \\
      = & \set{x_c + L u}{\norm{u}_2 \leq 1}
  \end{align*}
  where $P = L\tpose L \in S^n_{++}$ is a symmetric positive definite matrix.
  
\end{frame}

\begin{frame}{Norm Balls and Cones}

  \emph{Norm :}  a function $\norm{\cdot}$ that satisfies
  \begin{enumerate}
  \item $\norm{x}\geq 0$ and $\norm{x}=0 \iff x=0$
  \item $\norm{t x} = \abs{t} \cdot \norm{x}$ for $t\in\Re$
  \item $\norm{x+y} \leq \norm{x} + \norm{y}$
  \end{enumerate}

  \vfill
  \emph{Norm ball} with center $x_c$ and radius $r$ : $\set{x}{\norm{x-x_c}\leq r}$

  \vfill
  \begin{minipage}{0.6\textwidth}
    \emph{Norm cone} $\set{(x, t) \in E\times \Re}{\norm{x}\leq t}$

    \vspace{1em}
    Using $\norm{\cdot}_2:$
    \begin{itemize}
    \item Second-order cone
    \item Lorentz cone
    \item Ice-cream cone
    \end{itemize}
  \end{minipage}%
  \hfill
  \begin{minipage}{0.4\textwidth}
    \includegraphics[height=4cm]{norm-cone/norm-cone.pdf}
  \end{minipage}%
  
\end{frame}

\begin{frame}{Euclidian Ball}

  \textbf{Theorem :} Norm ball $B[x_c, r]$ is a convex set.

  \vfill

  \textbf{Proof [$\star$] : } Take any two points $x$ and $y$ in $B[x_c, r]$. Then for $\theta\in[0, 1]$, we have
  \begin{align*}
    \norm{\theta x + (1-\theta) y - x_c}  & =  \norm{\theta (x-x_c) + (1-\theta) (y-x_c) } \\
                                            & \leq \theta \norm{x-x_c} + (1-\theta) \norm{(y-x_c)}\\
                                            & \leq r
  \end{align*}
  where the first inequality follows from the triangle inequality and positive homogeneity of the norm.
\end{frame}


\begin{frame}{Positive Semidefinite Cone}

  $S^n$ is the space of \emph{symmetric} $n\times n$ \emph{matrices}

  \vfill

  $S^n_{++}=\set{X\in S^n}{X\succeq 0}$ : \emph{positive semidefinite} $n\times n$ \emph{matrices}
  \[
    X \in S^n_{++} \iff y\tpose X y \geq 0, ~\forall y\in\Re^n.
  \]

  \vfill
  
  \begin{minipage}{0.5\textwidth}
    \textbf{Example:}
    \[
      \begin{pmatrix}
        x & y\\
        y & z
      \end{pmatrix}
      \in S^2_+
    \]
    or
    \[
      x\geq 0, ~ z x \geq y^2.
    \]
  \end{minipage}%
  \hfill
  \begin{minipage}{0.5\textwidth}
    \includegraphics[height=4cm]{sdo-cone/sdo-cone.pdf}
  \end{minipage}
  
  
\end{frame}

\section{Operations that Preserve Convexity}

\begin{frame}{Establishing Convexity}

  How can we establish that a set $C$ is convex ?

  \vfill

  \textbf{Approaches :}
  \begin{enumerate}
  \item Check definition\\[1em]
  \item Reduction to basic convex sets using convex set algebra
  \end{enumerate}
  
\end{frame}

\begin{frame}{Intersection}

  \textbf{Theorem :} If $S_1$ and $S_2$ are convex sets then so is their \emph{intersection} $S_1\cap S_2$.

  \vspace{1em}
  
  \textbf{Theorem :} If $S_i$ are convex sets for $i$ in $I$ then so is their \emph{intersection} $\cap_{i\in I} S_i$.

  \vfill

  \textbf{Examples}:\\[0.5em]
  Consider the space of symmetric square matrices $S^{n}$.
  \begin{itemize}
  \item The cone $S_+^n$ of positive definite matrices is defined as
    \[
      \cap_{z\in\Re^n} \set{X\in S^n}{z\tpose X z\geq 0}.
    \]
  \item The cone $P_+^n$ of copositive matrices is define as
    \[
      \cap_{z\in\Re_+^n} \set{X\in S^n}{z\tpose X z\geq 0}.
    \]
  \end{itemize}

      
\end{frame}

\begin{frame}{Affine Transformations}
  

  A function $f:E_1\to E_2$ is \emph{affine} if it holds that
  \[
    f(p_1 x_1 + p_2 x_2) = p_1 f(x_1) + p_2 f(x_2)
  \]
  for all $p_1, p_2 \in \Re$ with $p_1+p_2=1$ and $x_1, x_2 \in E_1$.

  \vspace{1em}
  \textbf{Example:} 
  The function $f(x) = Ax - b$ with $A\in\Re^{m\times n}$ and $b\in\Re^m$ is affine.
  
  
  \vfill

  \uncover<2->{
  \textbf{Theorem :} The \emph{image} of a set $S \subseteq E_1$ through $f:E_1\to E_2$ is the set
  \[
    f(S) \defn \set{f(x) \in E_2}{x\in S}
  \]
  If $S$ is a convex set and $f$ is affine, then so is its image $f(S)$.
  }

  \vfill

  \uncover<3->{
  \textbf{Theorem : } The \emph{preimage} of a set $S \subseteq E_2$ through a function $f:E_1\to E_2$ is the set
  \[
    f^{-1}(S) \defn \set{x\in E_1}{f(x)\in S}
  \]
  If $S$ is a convex set and $f$ is affine, then so is its image $f(S)$.
  }

\end{frame}

\begin{frame}{Ellipsoidal Sets are Convex}

  
  \begin{minipage}{0.5\textwidth}
    An ellipsoidal set
    \[
      C = \set{x_c + L u}{\norm{u}_2 \leq 1}
    \]
    is convex.
  \end{minipage}%
  \hfill
  \begin{minipage}{0.5\textwidth}
    \centering
    \includegraphics[height=3.5cm]{ellipsoid/ellipsoid.pdf}
  \end{minipage}

  \vfill

  \textbf{Proof [$\star$]:}
  Define the affine function $f(x) = x_c + L x$ with $L\in\Re^{n\times m}$ and $x_c\in\Re^{n}$. The image set
  \[
    C = \set{f(x)}{x\in B[0, 1]} = f(B[0, 1])
  \]
  is convex.
\end{frame}

\begin{frame}{Polyhedral Sets are Convex}

  
  \begin{minipage}{0.5\textwidth}
    A \emph{polyhedral} set
    \[
      C = \set{x} {a_i\tpose x \leq b_i, \quad i \in [1, \dots, k]}
    \]
    is convex.
  \end{minipage}%
  \hfill
  \begin{minipage}{0.5\textwidth}
    \centering
    \includegraphics[height=3.5cm]{polyhedron/polyhedron.pdf}
  \end{minipage}

  \vfill

  \textbf{Proof [$\star$] :}
  Define the affine function $f(x) = b - Ax$ with $A\in\Re^{k\times n}$ and $b\in\Re^{k}$. The preimage set
  \[
    C = \set{x}{f(x)\in\Re^k_+} = f^{-1}(\Re_+^k)
  \]
  is convex.
\end{frame}



\section{Conic Inequalities}

\begin{frame}{Proper Cones}

  A cone $K$ in $E$ is called a \emph{proper cone} if it satisfies the following
  \begin{enumerate}
  \item K is convex
  \item K is closed
  \item K is solid, i.e., $\interior K \neq \emptyset$
  \item K is pointed, i.e., $K$ contains no line, equivalently $x\in K, ~-x \in K \implies x=0$.
  \end{enumerate}

\end{frame}

\begin{frame}{Generalized Inequalities}

  A proper cone can be used to define a \emph{partial order}
  \[
    x \preceq_{K} y \iff y-x \in K
  \]
  Similarly, we can define a \emph{strict partial order}
  \[
    x \prec_{K} y \iff y-x \in \interior K
  \]

  \vfill
  
  \textbf{Examples:}\\[0.5em]
  \begin{itemize}
  \item Let $E=\Re^n$ and $K=\Re^n_+$ the positive orthant. The order $x\preceq_{\Re_+} y$ means that $x_i\leq y_i$ for all $i$ in $[1, \dots, n]$.
  \item<2-> Let $E=\S^n$ and $K=\S^n_+$ the cone of positive semidefinite matrices. The order $X\preceq_{\S^n_+} Y$ means that $Y-X$ has positive eigenvalues.
  \end{itemize}
  
\end{frame}


\begin{frame}{Properties of Generalized Inequalities}

  The partial order $\preceq_{K}$ satisfies the properties
  \begin{itemize}
  \item $\preceq_{K}$ is preserved under addition : if $x \preceq_{K} y$ and $u \preceq_{K} v$, then
    $x+u\preceq_K y+v$
  \item $\preceq_{K}$ is preserved under nonnegative scaling : if $x \preceq_{K} y$ and $\alpha>0$, then $\alpha x \preceq_{K} \alpha y$
  \item $\preceq_{K}$ is transitive : if $x\preceq_{K} y$ and $y\preceq_{K}z$, then $x \preceq_{K} z$
  \item $\preceq_{K}$ is reflexive : $x\preceq_{K} x$
  \item $\preceq_{K}$ is anti-symmetric : $x\preceq_{K}y$ and $y \preceq_{K} x$ implies $x=y$
  \item $\preceq_{K}$ is preserved under limits : $x_i\preceq_{K} y_i$ for $i\in \N$, $x_i\to x$ and $y_i\to y$, then $x\preceq_{K} y$.
  \end{itemize}

  \vfill
  
  The strict partial order $\prec_K$ satisfies the properties
  \begin{itemize}
  \item $x \prec_K y \implies x\preceq_K y$
  \item $\prec_{K}$ is preserved under addition : if $x \prec_{K} y$ and $u \prec_{K} v$, then
    $x+u\prec_K y+v$
  \item $\prec_{K}$ is preserved under nonnegative scaling : if $x \prec_{K} y$ and $\alpha>0$, then $\alpha x \prec_{K} \alpha y$
  \item $x \not\prec_K x$
  \item $x\prec_K y$, then for $u$ and $y$ small enough, $x+u\prec_K y+v$
  \end{itemize}


  % \vfill
  
  % \textbf{Partial Order} : Not all points are comparable

\end{frame}

\begin{frame}{Minimum and Minimal Elements}

  The standard inequality $\leq$ on $\Re$ implies a \emph{linear ordering}. Either $x\geq y$ or $x\leq y$. A generalized inequality $x\preceq_K y$ on $E$ is only a \emph{partial ordering}.

  \vfill

  \begin{minipage}{0.5\textwidth}
    \begin{center}    \includegraphics[height=4cm]{minimum-element/minimum-element.pdf}
    \end{center}
    A point $x_1$ is \emph{the minimum element} if
    \[
      x_1\preceq_K y~~\forall y \in C.
    \]
  \end{minipage}%
  \hfill
  \begin{minipage}{0.5\textwidth}
    \begin{center}    \includegraphics[height=4cm]{minimal-element/minimal-element.pdf}
    \end{center}
    A point $x_1$ is \emph{a minimal element} if
    \[
      x_1\not\succeq_K y~~\forall y \in C\setminus\{x_1\}.
    \]
  \end{minipage}%

  
\end{frame}

\begin{frame}{Multiobjective Optimization}

  We write a generalized conic optimization problem as
  
  \[
    \begin{array}{rl}
      \minimize_{\preceq_K} & x \\[0.5em]
      \st & x \in C.
    \end{array}
  \]

  \vfill

  Solution types:
  
  \begin{itemize}
  \item \emph{Weak solutions} correspond to the set off all minimal elements.
  \item The \emph{strong solution} corresponds to the minimum element. 
  \end{itemize}
  
\end{frame}

\begin{frame}{Minimum Element -- Example: Bounding Box}

  Consider the set $R$ of all rectangles $\mc R = [a, b]\times [c, d]$ in the plane.

  Define a partial order $\mc R_1 \preceq \mc R_2 \iff \mc R_1\subseteq \mc R_2$ based on set inclusion.

  \vfill

  Find ``smallest rectangle'' containing a set $P$, i.e.,

  \[
    \begin{array}{rl}
      \minimize_{\preceq} & \mc R \\[0.5em]
      \st & \mc R \in S= \set{\mc R\in R}{P \subseteq \mc R}.
    \end{array}
  \]

  \vfill

  \uncover<2->{
  There is a strong solution.
  
  \emph{Minimum element} : bounding box $\mc R^\star = \cap_{\mc R\in S} \mc R$.
  }
  
  
\end{frame}

\begin{frame}{Minimal Elements -- Example: Minimal Ellipsoids}

  Consider the set $E$ of all centered ellipsoids $\mc E = \set{x}{x\tpose A x\leq 1}$ in the plane.

  Define a partial order $\mc E_1 \preceq \mc E_2 \iff \mc E_1\subseteq \mc E_2$ based on set inclusion.

  \vfill

  Find ``smallest ellipsoid'' containing a set $P$, i.e.,

  \[
    \begin{array}{rl}
      \minimize_{\preceq} & \mc E \\[0.5em]
      \st & \mc E \in S= \set{\mc E\in E}{P \subseteq \mc E}.
    \end{array}
  \]

  \vfill

  \uncover<2->{
    \begin{minipage}{0.5\textwidth}
      
      There is no strong solution.
      \begin{itemize}
      \item $\mc E_2, \mc E_3$ are both minimal.
      \end{itemize}
      
    \end{minipage}%
    \hfill
    \begin{minipage}{0.5\textwidth}

      \begin{center}
        \includegraphics[width=3cm]{smallest-ellipsoid.pdf}
      \end{center}
      
    \end{minipage}
  }

\end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
