using QHull
using CSV

srand(12)

n = 15
P = randn(n, 2)
points = DataFrame(x1=P[:, 1], x2=P[:, 2])

ch = chull(P)
vertices = DataFrame(x1=P[ch.vertices, 1], x2=P[ch.vertices, 2])

CSV.write("points.csv", points; quotechar=''')
CSV.write("vertices.csv", vertices; quotechar=''')
