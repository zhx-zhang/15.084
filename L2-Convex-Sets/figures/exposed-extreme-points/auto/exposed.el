(TeX-add-style-hook
 "exposed"
 (lambda ()
   (TeX-run-style-hooks
    "latex2e"
    "standalone"
    "standalone10"
    "import"
    "pgfplots"))
 :latex)

