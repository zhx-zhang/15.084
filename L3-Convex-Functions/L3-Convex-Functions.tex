\documentclass[9pt]{beamer}
% \documentclass[handout, 9pt]{beamer}

\usepackage{import}
\subimport{../}{preamble.tex}
\subimport{../}{beamer-theme.tex}

% Location for all figures / pictures
\graphicspath{{pictures/}{figures/}}

% Figures inline
\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{calc}
\usetikzlibrary{shapes.multipart}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{arrows.meta}
\usepgfplotslibrary{fillbetween}

\newcommand{\xbar}{1}

%--------------------------------------------------
% Presentation Information
%--------------------------------------------------
\title[15.084J/6.252J Nonlinear Optimization]{15.084J/6.252J : Convex Functions \& Optimality Conditions}
\date[]{Spring 2020}
%--------------------------------------------------

\begin{document}

\begin{frame}  

  \maketitle

  \vspace{-2em}
  
    \begin{center}
    \begin{tabular}{lll}
      Instructor : & \textbf{Bart Van Parys} & \textbf{(\href{mailto:vanparys@mit.edu}{vanparys@mit.edu})} \\
      TA : & Chenyang Yuan & (\href{mailto:ycy@mit.edu}{ycy@mit.edu})
    \end{tabular}
  \end{center}
  
\end{frame}

\begin{frame}{Previous Lecture}

  \begin{itemize}
  \item Convex Sets
  \item Algebra of Convex Sets
  \item Generalized Inequalities
  \end{itemize}
  
\end{frame}

\section{Convex Functions}

\begin{frame}{Convex Functions}

  Let $f:C\to\Re$.
  \begin{itemize}
  \item A function $f$ is \emph{convex} if its domain $\dom(f)=C$ is a convex set and
     \[
      \forall x, y\in C, ~\forall \theta \in [0, 1] : \quad f(\theta\cdot x+(1-\theta)\cdot y) \leq \theta \cdot f(x) + (1-\theta) \cdot f(y)
    \]
  \end{itemize}

  \vfill
  \includegraphics[height=5cm]{convex-function/convex-function.pdf}
  
\end{frame}


\begin{frame}{Extended Value Functions}

  Consider a function $f$ with domain $\dom(f) \subset E$. It is often convenient to extend the function to the entirety of $E$. Denote
  \[
    \tilde f(x)\defn
    \begin{cases}
      f(x) & {\rm{if~}} x\in\dom(f),\\[0.5em]
      +\infty & {\rm{otherwise.}}
    \end{cases}
  \]
  The function $\tilde f:E\to\Re\cup\{+\infty\}$ is an \emph{extended value function}. 

  \vfill
  
  \textbf{Properties:}
  \begin{itemize}
  \item Extended arithmetic: $x<\infty$, $x+\infty=\infty$ for all $x\in \Re$; also $0\cdot\infty=0$.
  \item<2-> The \emph{domain} of the function $\tilde f$ is $\dom(\tilde f)=\tset{x\in E}{\tilde f(x) < \infty} = \dom(f)$.
  \item<3-> A function $f$ is \emph{convex} if 
    \[
      \forall x, y\in E, ~\forall \theta \in [0, 1] : \quad f(\theta x+(1-\theta) y) \leq \theta f(x) + (1-\theta) f(y)
    \]

  \end{itemize}
  
\end{frame}


\begin{frame}{Epigraph and Sublevel set}

  \emph{Sublevel set} of a function $f:E\to\Re$:
  \[
    \set{x \in E}{f(x)\leq \alpha}
  \]
  Sublevel sets of convex functions are convex sets (converse is false).

  \vfill

  \uncover<2->{
  \emph{Epigraph} of a function $f:E\to\Re$:
  \[
    \epi(f) \defn \set{(x, t)\in E\times\Re}{f(x)\leq t}
  \]

  \begin{center}
    \includegraphics[height=3cm]{epigraph/epigraph.pdf}
  \end{center}
  }

  \uncover<3->{
    \textbf{Theorem :} A function $f$ is convex $\iff$ its epigraph $\epi(f)$ is a convex set.
    }
\end{frame}


\begin{frame}{Jensen's Inequality}

  \begin{minipage}{0.5\textwidth}
    \includegraphics[height=3cm]{jensen.jpg}\\
    Jensen
  \end{minipage}

  \vfill
  
  \textbf{Theorem : }Let $x_i\in E$ be a finite collection $i\in I$ of points. Then for $f:E\to\nRe$ a convex function
  \[
    f\left(\textstyle\sum_{i\in I} p_i \cdot x_i\right)\leq \textstyle\sum_{i\in I} p_i \cdot f(x_i) 
  \]
  with $p_i\geq 0$ for all $i\in I$ and $\sum_{i\in I} p_i=1$.

  \vfill
  
  \textbf{Remark :} Holds much more general as $f(\E{\mb P}{X}) \leq \E{\mb P}{f(X)}$.
\end{frame}

\begin{frame}{Strictly Convex Functions}
  
  \begin{itemize}
  \item A \emph{strictly convex} function $f : E\to\nRe$ satisfies
    \[
      \forall x, y\in E,~ \forall \theta \in (0, 1) : \quad f(\theta\cdot x+(1-\theta)\cdot y) < \theta f(x) + (1-\theta) f(y)
    \]
  \item A function $f$ is \emph{(strictly) concave} if $-f$ is (strictly) convex
  \end{itemize}

  \vfill
  \textbf{Non-strictly convex function:}\\[0.5em]
  \begin{minipage}{0.5\textwidth}
    \centering
    \includegraphics[height=3.5cm]{convex-function/strictly-convex-function.pdf}
  \end{minipage}%

  
\end{frame}

\section{Common Convex Functions}


\begin{frame}{Elementary Convex Functions}

  \textbf{Convex Functions}
  \begin{itemize}
  \item Affine functions, e.g.\
    \[
      f(x) = A x - b.
    \]
    Convex on $\Re^n$ for any $A\in\Re^{m\times n}$ and $b\in\Re^m$.
  \item<2-> Exponential functions
    \[
      f(x) = \exp(a x).
    \]
    Convex on $\Re$ for any $a\in\Re$.
  \item<3-> Power functions
    \[
      f(x) = x^a.
    \]
    convex on $\Re_{++}$ when $a\in (-\infty, 0]\cup [1, \infty)$.
  \item<4-> Log-sum-exp functions
    \[
      f(x) = \log\left(\sum_{i=1}^k \exp(a_i\tpose x)\right).
    \]
    Convex on $\Re^n$ for $a_i\in\Re^n$.
  \end{itemize}
  
\end{frame}

\begin{frame}{Elementary Concave Functions}

  \textbf{Concave Functions}
  \begin{itemize}
  \item Affine functions, e.g.\
    \[
      f(x) = A x - b.
    \]
    Concave on $\Re^n$ for any $A\in\Re^{m\times n}$ and $b\in\Re^m$.
  \item<2-> Geometric mean
    \[
      f(x) = \left(\prod_{i=1}^n x_i\right)^{1/n}
    \]
    Concave on $\Re^n_{+}$.
  \item<3-> Entropy
    \[
      f(x) = \sum_{i=1}^n x_i \log(1/x_i)
    \]
    Concave on $\Re_+^n$.
  \item<4-> Log-determinant
    \[
      f(X) = \log(\det(X)).
    \]
    Concave on $S^{n}_{++}$.
  \end{itemize}

  
\end{frame}


\begin{frame}{Characteristic and Minkowski Functions}

  Let the set $C$ be a convex set. The following functions are convex.

  \begin{itemize}
  \item The \emph{characteristic function} of a set $C$ is defined as
    \[
      \chi_{C}(x) \defn \begin{cases} 0 & \mathrm{if}~x\in C, \\ +\infty & \mathrm{otherwise}. \end{cases}
    \]
    \vspace{2em}
  \item The \emph{Minkowski function} of a set $C$ is defined as
    \[
      \mu_{C}(x) \defn \inf\set{t>0}{x/t\in C}
    \]
  \end{itemize}

  %TODO: Proof that Minkowski function is convex.
  
  \vfill
  
  \textbf{Theorem :} Minkowski function is
  \begin{itemize}
  \item $\mu_C(x) \leq 1$ for all $x\in C$ and $\mu_C(x) < 1$ for all $x \in \interior C$.
  \end{itemize}
  
\end{frame}

\begin{frame}{Minkowski Function}

  \centering
  \includegraphics[height=8cm]{minkowski-function.pdf}

\end{frame}


\section{Operations that Preserve Convexity}

\begin{frame}{Operations that Preserve Convexity}

  How to establish whether a function is convex?

  \vfill

  \textbf{General Approaches :}
  \begin{enumerate}
  \item Verify definition
  \item Reduction to elementary convex functions using convex function algebra
  \end{enumerate}

\end{frame}

\begin{frame}{Positive weighted sum}

  \textbf{Theorem :} The function $f_1 + f_2$ is convex if $f_1$, $f_2$ are convex\\[1em]

  \vfill

  \textbf{Proof :} we have
  \begin{align*}
    (f_1+f_2)(\theta x + (1-\theta) y) &= f_1(\theta x + (1-\theta) y) + f_2(\theta x + (1-\theta) y)\\
                                       &\leq \theta f_1( x )+ (1-\theta) f_1(y) + \theta f_2( x )+ (1-\theta) f_2(y) \\
    & \leq \theta (f_1(x) + f_2(x)) + (1-\theta)(f_1(y) + f_2(y)) 
  \end{align*}
  for all $x, y$ and $\theta \in [0, 1]$.
  
  \vfill

  \textbf{Examples :}
  \begin{itemize}
  \item $f(x) = x^2 +\abs{x}$ is convex as both $x^2$ and $\abs{x}$ are convex functions
  \item $f(x) = a \exp(-x)$ is convex when $a\geq 0$ and concave when $a\leq 0$ 
  \end{itemize}
  
\end{frame}

\begin{frame}{Composition with Affine Functions}

  Let $f:E_1\to E_2$ be an affine function. Suppose $g:E_2\to\nRe$ is a convex function.\\[1em]

  \textbf{Theorem : } Define the composition $h:E_2\to\nRe$ as
  \[
    h(x) = (g \circ f)(x) = g(f(x)).
  \]
  Then, the $h$ is a convex function as well.

  \vfill
  
  \textbf{Examples :}
  \begin{itemize}
  \item $h(x) = -\log(a\tpose x + b)$ is convex with domain $a\tpose x + b > 0$
  \end{itemize}

  
\end{frame}

\begin{frame}{Point-Wise Maximum}

  \textbf{Theorem :} If $f_i$ are convex for all $i$ in $I$, then the \emph{point-wise supremum} $$f(x) \defn \sup_{i\in I} \, f_i(x)$$ is convex as well.

  \vfill
  
  \textbf{Examples :}
  \begin{itemize}
  \item Sum of $k$ largest components
    \begin{align*}
      f(x) &= x_{[1]} + x_{[2]} + \dots+ x_{[k]} \\
      \intertext{where $x_{[i]}$ is $i$-th largest component of $x$}
      f(x) &= \max \set{x_{i_1} + x_{i_2} + \dots +  x_{i_k}}{\rm{distinct}~ i_1, \dots, i_k \in [1, \dots, n]}
    \end{align*}
  \item<2-> Maximum eigenvalue
    $f(X)=\lambda_{\max}(X) = \max_{\norm{v}=1} v\tpose X v$.
  \item<3-> Let $f(\cdot, y)$ be convex for all $y$. Then,
    \[
      g(x) \defn \sup_{y \in S} \, f(x, y)
    \]
    is convex for any arbitrary $S$.
  \end{itemize}

\end{frame}

\begin{frame}{Partial Minimization}

  \textbf{Theorem :} If $f(x, y):E_1\times E_2\to\nRe$ is jointly convex in $(x, y)$ and $C \subseteq E_1$ a convex set, then
  \[
    g(y) \defn \inf_{x\in C} f(x, y)
  \]
  is convex.

  \vfill
  \uncover<2->{
  \textbf{Proof [$\star$]:} The proof follows from
  \begin{align*}
    \epi(g) \defn & \set{(y, t)\in E_1\times \Re}{g(y)\leq t} \\
            = & \cap_{\epsilon>0}\set{(y, t)\in E_1\times \Re}{\exists x\in C ~:~ f(x, y)\leq t+\epsilon} \\
            = & p(\cap_{\epsilon>0} \set{(x, y, t)\in E_1\times C\times \Re}{f(x, y)\leq t+\epsilon})\\
            = & p(\set{(x, y, t)\in E_1\times C\times \Re}{f(x, y)\leq t})\\
            = & p(\set{(x, y, t)\in E_1\times C\times \Re}{(x, y, t)\in\epi(f)})
  \end{align*}
  where $p:(x, y, t) \mapsto (y, t)$ is a linear projection function.
  }
\end{frame}

\begin{frame}{Example}

  \textbf{Set distance :} The distance to a convex set $C$ defined as
  \[
    g(y) \defn \inf_{x\in C} \, \norm{x-y}
  \]
  is a convex function.

\end{frame}

\begin{frame}{Perspective}

  The \emph{perspective} of a function $f : E \to \Re$ is the function $g:E\times\Re \to\Re$
  \[
    g(x, t) \defn t \cdot f(x/t),
  \]
  with $t>0$.

  \vfill

  \textbf{Theorem : } If $f$ is a convex function, then so is its perspective.

  \vfill

  \textbf{Kullback-Leibler divergence} The Kullback-Leibler divergence between $p\in\Re^n_{++}$ and $q\in\Re^n_{++}$ defined as
  \begin{align*}
    D_{\rm{kl}}(p, q)   & = \sum_{i=1}^n p_i \log\left(\frac{p_i}{q_i}\right) - p_i + q_i \\[0.5em]
                      & = \sum_{i=1}^n p_i [-\log]\left(\frac{q_i}{p_i}\right) - p_i + q_i
  \end{align*}
  is convex.
  
\end{frame}

% \begin{frame}{Example}
% \end{frame}

\section{Continuous Functions}

\begin{frame}{A Brief Recall of Continuity}  

  Let $f:\Re^n\to\nRe$.
  \begin{itemize}
  \item A function $f$ is called \emph{continuous at a point} $x$ in $\dom(f)$ if for every sequence
    $\{x_i\}_{i\geq 0} \subset \Re^n$ converging to $x$, we have $\lim_{i\to\infty} f(x_i) = f(x)$\\[2em]

  \item A function $f$ is called \emph{continuous} if it is continuous at all points of its domain.
  \end{itemize}

  \vfill
  
  \textbf{Counter example:}

  \vfill

  \includegraphics[height=3cm]{Left-continuous.pdf}

  \hfill
  {\tiny Figure from Wikipedia}
  
\end{frame}

\begin{frame}{Extreme-Value Theorem (1830s)}

  \textbf{Theorem (Bolzano-Weierstra\ss) :} If $K \subset \Re^n$ is a compact (bounded and closed) set and $f : K \to \Re$ is a continuous function, then $f$ is bounded and there exist $c$, $d \in K$ such that $f ( c ) = \sup_{x\in K} f ( x )$ and $f ( d ) = \inf_{x \in K} f ( x )$.

  \vfill

  \includegraphics[height=5.5cm]{Extreme_Value_Theorem.pdf}

  \hfill
  {\tiny Figure from Wikipedia}
  
\end{frame}

\begin{frame}{Convex Functions are not Continuous}

  Consider the convex function
  \[
    f(x) \defn\left\{
      \begin{array}{rl}
        0 & {\rm{if~}} x_1^2+x_2^2 < 1 \\
        1 & {\rm{if~}} x_1^2+x_2^2 = 1 ~\&~ x_1\neq -1 \\
        2 & {\rm{if~}} x_1=-1, x_2=0 \\
        +\infty & {\rm{otherwise}}
      \end{array}\right.
  \]

  \vfill
  
  Properties:
  \begin{itemize}
  \item $f$ is a convex function with domain $\dom(f) = \set{x\in\Re^2}{x^2_1+x^2_2 \leq 1}$
  \item $f$ is continuous on $\interior(\dom (f))$
  \item $f$ is discontinuous at $x=(-1, 0)$
  \end{itemize}

\end{frame}

\begin{frame}{Convex Functions are Irregular at Extreme Points}

  \begin{center}
    \includegraphics[height=4cm]{convex-discontinuity/convex-discontinuity.pdf}
  \end{center}
  
\end{frame}

\begin{frame}{Convex Functions are Almost Continuous}

  \textbf{Theorem :} A convex function $f:\Re^n\to\nRe$ is continuous on $\interior(\dom(f))$.\\[1em]

  \uncover<2->{
    \textbf{Proof [$\star$] :}

    \begin{enumerate}
    \item \emph{$f$ is locally bounded on $\interior(\dom(f))$}. Let $x \in \interior(\dom(f))$, $P\defn \{x_0, \dots, x_n\}$ points such that $x \in \Delta\defn \conv(P)$, $M \defn \max_{0\leq i\leq n} f(x_i)$. Then $f(y)\leq M$ for all $y \in \Delta$ by convexity of $f$. Let $y \in B\defn B(x, \epsilon)\subseteq \Delta$, then $2 x -y \in B$ and $f(x) \leq [f(2x -y) + f(y)]/2 \leq [M + f(y)]/2$. Hence $m\defn 2 f(x) - M \leq f(y) \leq M$ for any $y\in B$.
    \item \emph{$f$ is locally Lipschitz continuous on $\interior(\dom(f))$}, i.e., $\forall x \in\interior(\dom(f))$, $\exists U \ni x$ an open set, $\exists L>0$ : $\forall y, z \in U$ such that $\abs{f(y)-f(z)}\leq L \norm{y-z}$. Let $U\defn B(x, \epsilon/2)$, $y\neq z \in U$, $y'\defn y+\epsilon (y-z)/2 \norm{y-z}\in B$ so that $y = (1-\lambda) z + \lambda y'$ for some $\lambda \leq 2 \norm{y-z}/\epsilon$. Define $\Gamma = \max \{m, M\}$. Then, $\abs{f(y)-f(z)}\leq \abs{\lambda f(y') + (1-\lambda) f(z) -f(z)} = \lambda \abs{f(y')-f(z)}\leq 4 \Gamma \norm{y-z}/\epsilon$.
    \item \emph{Locally Lipschitz continuous functions are continuous}. Let $(x_i)_{i\geq 0} \in U$ converge to $x\in U$. As $\abs{f(x_i) - f(x)} \leq L \norm{x_i-x} \to 0$, $f(x_i) \to f(x)$ and $f$ is continuous at $x$.
    \end{enumerate}
  }
  
\end{frame}

\section{Differentiable Functions}

\begin{frame}{A Brief Recall of Differentiability}


  Let $f:\Re^n\to\nRe$ continuous and $d\in \Re^n$.

  \begin{itemize}
    \setlength\itemsep{2em}
  \item The function $f$ is \emph{differentiable} at $x \in \dom(f)$ \emph{in the direction} $d$ if the following limit exists:
    \[
      \nabla f(x)[d] = \lim_{t\to 0} \frac{f(x+t d)-f(x)}{t}.
    \]
  \item<2-> The function $f$ is \emph{G\^ateaux differentiable} in $x \in \dom(f)$ if differentiable in $x$ in every direction $d \in \Re^n$ and if $d\mapsto \nabla f(x)[d]$ is linear.

  \item<3-> The \emph{gradient} of $f$ at $x$ is the vector $f'(x)$ satisfying $\iprod{f'(x)}{d}=\nabla f(x)[d]$.
  \item<4-> $f$ is \emph{continuously differentiable} if $x \mapsto f'(x)$ is continuous
  \end{itemize}
  
  
\end{frame}

\begin{frame}{Convex Functions are Almost Differentiable}

  \textbf{Theorem : } Let $f:\Re^n\to\nRe$ be convex, $x\in \interior(\dom(f))$ and $d\in\Re^n$. Then, all directional derivatives
  \[
    \nabla f(x)[d] \defn \lim_{t\downarrow 0} \frac{f(x+td)-f(x)}{t} \rm{~exist}.
  \]


  \vfill

  \textbf{Remark :} $\nabla f(x)[d]$ might not be linear in $d$. For instance, $f(x)=\abs{x}\in\Re$ with $\nabla f(0)[d]=\abs{d}$.


  \vfill

  \uncover<2->{
  \textbf{Proof [$\star$] : }
  Let $\mu(t)\defn {(f(x+td)-f(x))/}{t}$ for all $t>0$. The function $\mu$ is increasing as $f$ is convex. Furthermore, the function $f$ is locally Lipschitz on $\interior(\dom(f))$ (See slide 26). Therefore, $\mu(t)$ is bounded from below by $-L(x) \norm{d}$ for $t>0$. Since $\mu(t)$ decreases when $t\downarrow 0$ but is bounded from below, a finite limit $\lim_{t\downarrow 0}\mu(t)$ must exists.}
\end{frame}
  
\begin{frame}{Differentiable Convex Functions}

  Let $f:\Re^n\to\nRe$ be a differentiable convex function. For any two points $x, y\in\dom(f)$ we have
  \begin{align*}
    \iprod{f'(x)}{y-x} & = \lim_{t\to 0} \frac{f(x + t(y-x))-f(x)}{t} \\
                       & \leq \lim_{t\to 0} \frac{(1-t)f(x) + t f(y)-f(x)}{t} = f(y) - f(x).
  \end{align*}
  
  \vfill
  \textbf{Gradient Property :}
  \[
    f(y) \geq f(x) + \iprod{f'(x)}{y-x} \quad \forall x, y\in\dom(f).
  \]

  \vfill

  \begin{center}
    \begin{tikzpicture}
      \draw[draw=blue, thick] plot[id=ex1,domain=-4:4,samples=1000] function {cosh(0.4*x)};
      % \fill[blue!5] plot[id=ex2,domain=-4:4,samples=1000] function {cosh(0.4*x)} -- (4, 3) -- (-4, 3) -- cycle;

      \node[above] at ($(4, {cosh(0.4*4)})+(-0.4, -0.1)$) {\color{blue} $f$};
      
      \draw[->] (-4, 0) -- (4, 0);
      \node[right] at (4, 0) {$\Re^n$};
      % \node[above] at (-4, 3) {$\Re_+$};
      \draw[->] (-4, 0) -- (-4, 3);

      % Lazy constraint 1
      \def\sa{-2}
      \fill[red] (\sa, 0) circle [radius=2pt];
      \draw[densely dotted, <->] (\sa, 0) -- ($(\sa, {cosh(0.4*\sa)})$);
      \draw[draw=red,line width=0.2mm] plot[id=lc1,domain=-4:1.5,samples=1000] function {0.4*sinh(0.4*\sa)*(x-\sa)+cosh(0.4*\sa)};
      \node[below] at (\sa, -.1) {\red{$x$}};
    \end{tikzpicture}
  \end{center}

\end{frame}

\section{Unconstrained Optimality Conditions}

\begin{frame}{The Mean Value Theorem}

  Let $f:\Re^n\to\nRe$ be differentiable.

  \vfill
  \textbf{Theorem :} Let $x \in \dom(f)$ and $h\in \Re^n$ such that $x+h\in \dom(f)$. There is a $t\in(0, 1)$ for which
  \[
    f(x+h) = f(x) + \iprod{f'(x+t h)}{ h}.
  \]

  \vfill

  \textbf{Geometrically:}
  \vfill
  
  \includegraphics[height=4cm]{mean-value-theorem.pdf}

  \hfill
  {\tiny Figure from Wikipedia}
  
  
\end{frame}

\begin{frame}{Rolle's Theorem [$\star$]}

    \textbf{Proof [$\star$] :} Follows from Rolle's Theorem. Let $g:[a, b]\to\Re$ differentiable such that $g(a)=g(b)$. Then, there is a $c\in (a, b)$ such that $g'(c)=0$. Assume without loss of generality that there is a $t\in(a, b)$ for which $g(t)>g(a)$ and take $a\neq b\neq c\defn \arg \max \set{g(t)}{t \in [a, b]}$ (use the extreme value theorem). We have
  \[
    \frac{g(c+t)-g(c)}{t} \leq 0 {\rm{~for~}} t\in(0, b-c] {\rm{~and~}} \frac{g(c+t)-g(c)}{t} \geq 0 {\rm{~for~}} t \in [a-c, 0).
  \]
  As $g$ is differentiable, the limit exists when $t\to 0$ and hence $g'(c)=0$. Take now $g(t) = f(x+th)+t(f(x)-f(x+h))$ and $a=0$, $b=1$.  
  
\end{frame}

\begin{frame}{First-Order Optimality Conditions}

  
  \textbf{Theorem : } Suppose that $f$ is continuously differentiable and there exists a minimizer $x^\star \in \interior(\dom(f))$. Then, $f'(x^\star) = 0$.

  \vfill
  \textbf{Proof : }

  \begin{itemize}
  \item For contradiction, assume that $f'(x^\star)\neq 0$
  \item<2-> Let $g(t)\defn f(x^\star-t f'(x^\star))$ with gradient $g'(t) = -\iprod{f'(x^\star-t f'(x^\star))}{f'(x^\star)}$
  \item<3-> Then $g'(0)=-\iprod{f'(x^\star)}{f'(x^\star)}<0$
  \item<4-> By continuity of $f'$ there exists a $T>0$ such that $g'(t)<0$ for all $t\in [0, T]$.
  \item<5-> By the mean value theorem, there is an $t^\star\in (0, T)$ such that
    \[
      f(x^\star-T f'(x^\star))-f(x^\star) = -T \iprod{f'(x^\star-t^\star f'(x^\star))}{f'(x^\star)} = T g'(t^\star) <0,
    \]
  \item<6-> Inequality $f(x^\star-T f'(x^\star))<f(x^\star)$ contradicts minimality of $x^\star$
  \end{itemize}
 
\end{frame}

\begin{frame}{Example}

  \begin{itemize}
    \setlength\itemsep{2.5em}
  \item $f(x) =\frac{1}{2} x_1^2 +x_1.x_2 +2 x_2^2 -4x_1-4 x_2-x_2^3$\\
  \item $f'(x)= (x_1+x_2-4,~x_1+4x_2-4-3x_2^2)$. \\
  \item \emph{Stationary points}: $\bold{x}^*=(4,0)$ and $\bar{ \bold{x}}=(3,1)$
  % \item $f''( \bold{x}) =\left[
  %     \begin{array}{ccc}
  %       1  & & 1   \\
  %       1 & & 4-6 x_2
  %     \end{array}
  %   \right]$
  % \item $f''(\bold{x}^*) =\left[
  %     \begin{array}{ccc}
  %       1  & & 1   \\
  %       1 & & 4
  %     \end{array}
  %   \right]\succeq \bold{0}$
  % \item $f''(\bar{\bold{x}}) =\left[
  %     \begin{array}{ccc}
  %       1  & & 1   \\
  %       1 & & -2
  %     \end{array}
  %   \right]\not\succeq \bold{0}$
  % \item $x^\star$ is the only \textbf{candidate} local minimum
  \end{itemize}
\end{frame}


\begin{frame}{Second-Order Differentiability}

  Let $f:\Re^n\to\nRe$ be differentiable.
  \begin{itemize}
    \setlength\itemsep{1em}
  \item If $f_i'(x)$ are themselves differentiable at $x$ for each $i$, then their gradients at $x$ form the \emph{Hessian matrix} $f''(x) \in \Re^{n\times n}$.
  \item If $x\to f''(x)$ is continuous, \emph{Hessian matrix} $f''(x)\in S^n$ \emph{is symmetric} for each $x\in \interior(\dom(f))$. 
  \end{itemize}

  \vfill
  
  \textbf{Example:}
  \begin{itemize}
    \setlength\itemsep{1em}
  \item $f(x) =\frac{1}{2} x_1^2 +x_1.x_2 +2 x_2^2 -4x_1-4 x_2-x_2^3$\\
  \item $f'(x)= (x_1+x_2-4,~x_1+4x_2-4-3x_2^2)$. \\
  \item $f''( \bold{x}) =\left[
      \begin{array}{ccc}
        1  & & 1   \\
        1 & & 4-6 x_2
      \end{array}
    \right]$
  \end{itemize}

  
\end{frame}


\begin{frame}{Taylor Expansion Theorem}

  \textbf{Theorem : } Let $x\in \dom(f)$ and $h\in \Re^n$ such that $x+h\in\dom(f)$ and $f$ twice differentiable. There is a $t\in (0, 1)$ for which
  \[
    f(x+h) = f(x) + \iprod{f'(x)}{h} + \frac12 \iprod{f''(x+th) h}{h}.
  \]

  \vfill
  
  \textbf{Proof [$\star$] : } Let $R=f(x+h)-f(x)-\iprod{f'(x)}{h}$ and $g(t) \defn f(x+t h)+(1-t)\iprod{f'(x+t h)}{h} + Rt^2$. Then $g(1)-g(0)=2R=g'(t)$ for a $t\in (0, 1)$ because of the mean value theorem. Thus $g'(t) = (t-1)\iprod{f''(x+th)h}{h} + 2 R t = 2R$. It remains to simplify.

\end{frame}



\begin{frame}{Second-Order Necessary Condition}

  \textbf{Theorem : } Suppose that $x^\star \in \interior(\dom(f))$ is a local minimum of $f$ twice continuously differentiable. Then, $f'(x^\star)=0$ and $f''(x^\star)\in S^n_{+}$.

  \vfill
  
  \textbf{Proof :}
  \begin{itemize}
  \item For contradiction assume $\iprod{f''(x^\star) h}{h} < 0$ for some direction $h$
  \item<2-> Let $g(t) \defn f(x^\star+t h)$, $g'(t) = \iprod{f'(x^\star+t h)}{h}$ and $g''(t) = \iprod{f'(x^\star+t h) h}{h}$
  \item<3-> As $g''(0)<0$ and continuity of $g''$ there is a $T>0$ such that $g''(t)<0$ for all $t\in [0, T]$.
  \item<4-> Thus, $g(T)= g(0)+g'(0) T + g''(t^\star) T^2/2 < f(x^\star)$ for some $t^\star$; a contradiction
  \end{itemize}
  
\end{frame}

\begin{frame}{Example}

  \begin{itemize}
    \setlength\itemsep{1.5em}
  \item $f(x) =\frac{1}{2} x_1^2 +x_1.x_2 +2 x_2^2 -4x_1-4 x_2-x_2^3$\\
  \item $f'(x)= (x_1+x_2-4,~x_1+4x_2-4-3x_2^2)$. \\
  \item Stationary points: $\bold{x}^*=(4,0)$ and $\bar{ \bold{x}}=(3,1)$
  \item $f''( \bold{x}) =\left[
      \begin{array}{ccc}
        1  & & 1   \\
        1 & & 4-6 x_2
      \end{array}
    \right]$
  \item $f''(\bold{x}^*) =\left[
      \begin{array}{ccc}
        1  & & 1   \\
        1 & & 4
      \end{array}
    \right]\succeq_{S^2_+} \bold{0}$
  \item $f''(\bar{\bold{x}}) =\left[
      \begin{array}{ccc}
        1  & & 1   \\
        1 & & -2
      \end{array}
    \right]\not\succeq_{S^2_+} \bold{0}$
  \item $x^\star$ is the only \emph{candidate local minimum}.
  \end{itemize}
  
\end{frame}


\begin{frame}{Second-Order Sufficient Condition}

  \textbf{Theorem : } Suppose that $x^\star \in \interior(\dom(f))$ satisfies $f'(x^\star)=0$ and $f''(x)\in S^n_{++}$ and $f$ is twice continuously differentiable. Then, $x^\star$ is a local minimum of $f$.

  \vfill

  \textbf{Proof : }

  \begin{itemize}
  \item For contradiction assume that $x^\star$ is not a local minimizer of $f$
  \item<2-> For every $\epsilon > 0$, there exists $x^\star_\epsilon\in B(x^\star, \epsilon)$ for which
    \[
      f(x^\star) > f(x^\star_\epsilon) = f(x^\star) + \frac12 \iprod{f''(x^\star+t_\epsilon (x_\epsilon^\star-x^\star)) (x_\epsilon^\star-x^\star)}{x_\epsilon^\star-x^\star}.
    \]
  \item<3-> Hence, $\lambda_{\min}(f''(x^\star+t_\epsilon (x_\epsilon^\star-x^\star))) < 0$ for every $\epsilon > 0$
  \item<4-> By continuity of $\lambda_{\min}$ and of $f''$ we deduce that $\lambda_{\min}(f''(x^\star))\leq 0$; a contradiction.
  \end{itemize}
  
\end{frame}

\begin{frame}{Example}

  \begin{itemize}
    \setlength\itemsep{1.5em}
  \item $f(x) =\frac{1}{2} x_1^2 +x_1.x_2 +2 x_2^2 -4x_1-4 x_2-x_2^3$\\
  \item $f'(x)= (x_1+x_2-4,~x_1+4x_2-4-3x_2^2)$. \\
  \item Candidates $\bold{x}^*=(4,0)$ and $\bar{ \bold{x}}=(3,1)$
  \item $f''( \bold{x}) =\left[
      \begin{array}{ccc}
        1  & & 1   \\
        1 & & 4-6 x_2
      \end{array}
    \right]$
  \item $f''(\bold{x}^*) =\left[
      \begin{array}{ccc}
        1  & & 1   \\
        1 & & 4
      \end{array}
    \right]\succ_{S^2_+} \bold{0}$
  \item $x^\star$ is the only local minimum.
  \end{itemize}
\end{frame}


\begin{frame}{First-order Optimality for Convex Functions}

  \textbf{Theorem : } Let $f:\Re^n\to\nRe$ be a continuously differentiable convex function with $\dom(f)=\interior(\dom (f))$. Then,
  \[
    x^\star \in \arg\min_{x\in \Re^n} f(x) \iff  f'(x^\star) =0.
  \]

  \textbf{Proof :} We have 
  \[
    f(y) \geq f(x^\star) + \iprod{0}{y-x^\star} =f(x^\star)
  \]
  for any $y \in \Re^n$ if $f'(x^\star)=0$. The other directions was proven for continuously differentiable functions in general.

\end{frame}


\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
