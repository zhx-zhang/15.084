\documentclass[9pt]{beamer}
% \documentclass[notes, handout, 9pt]{beamer}
% \documentclass[handout, 9pt]{beamer}

\usepackage{import}
\subimport{../}{preamble.tex}
\subimport{../}{beamer-theme.tex}

\usepackage{amsmath}

% Location for all figures / pictures
\graphicspath{{pictures/}{figures/}}

% Figures inline
\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{calc}
\usetikzlibrary{shapes.multipart}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{arrows.meta}
\usepgfplotslibrary{fillbetween}

\newcommand{\xbar}{1}

%--------------------------------------------------
% Presentation Information
%--------------------------------------------------
\title[15.084J/6.252J Nonlinear Optimization]{15.084J/6.252J : Modeling \& Applications}
\author[]{\textbf{Bart P.G. Van Parys}}
\date[]{}
\institute[]{MIT Sloan School of Management}
%--------------------------------------------------

\begin{document}

\begin{frame}  

  \maketitle

  \vspace{-2em}
  
    \begin{center}
    \begin{tabular}{lll}
      Instructor : & \textbf{Bart Van Parys} & \textbf{(\href{mailto:vanparys@mit.edu}{vanparys@mit.edu})} \\
      TA : & Chenyang Yuan & (\href{mailto:ycy@mit.edu}{ycy@mit.edu})
    \end{tabular}
  \end{center}
  
\end{frame}


\section{Optimization Formulations}

\begin{frame}{Standard Form Problems}

  \textbf{Nonlinear Optimization:}
  \[
    \begin{array}{rl}
      \minimize & f(x) \\[0.5em]
      \st & g_i(x)\leq 0,\quad \forall i\in[1,\dots, m]\\[0.5em]
                & h_i(x) = 0, \quad \forall i \in [1,\dots, \ell].
    \end{array}
  \]

  \vfill

  \textbf{Convex Optimization:}
  \begin{itemize}
  \item {Cost function} $f$ and {inequality constraint functions} $g_i$ are \emph{convex} functions.
  \item {Equality constraint functions} $h_i$ are \emph{affine} functions.
  \end{itemize}
                  
\end{frame}

\begin{frame}{Terminology}

  \emph{Solving} a \emph{nonlinear optimization} problem :
  \vfill
  \[
    \begin{array}{rl}
      \minimize & f(x) \\[0.5em]
      \st & x \in E \\[0.5em]
                & x\in X \subseteq E
    \end{array}
  \]
  \vfill
  means determining
  \begin{itemize}
  \item the infimum (greatest lower bound)
    \[
      \begin{array}{rl}
        J^\star = \inf_x & f(x) \\[0.5em]
        \st & x\in X \subseteq E
      \end{array}
    \]
  \item the set of \red{$\epsilon$-}minimizers
    \(
      \arg \min_{x\in X} f(x) \defn \set{x\in X\subseteq E}{f(x)\leq J^\star\red{+\epsilon}}
    \)
  \end{itemize}
  
\end{frame}


\begin{frame}{Equivalence}

  Two optimization problems are \emph{equivalent} if the solution of one problem can ``\emph{efficiently}'' be constructed from the solution of the other and visa versa.

  \vfill

  \textbf{Example:}
  \begin{minipage}{0.5\textwidth}
    \[
      \begin{array}{rl}
        \minimize & f(x)\\[0.5em]
        \st & x\in\Re^n.
      \end{array}
    \]
    \vspace{0.7em}
  \end{minipage}%
  \vline
  \begin{minipage}{0.5\textwidth}
    \[
      \begin{array}{rl}
        \minimize & f(x) + 5\\[0.5em]
        \st & x\in\Re^n, ~y\in \Re^n \\[0.5em]
                  & x+y\geq \bold{1}.
      \end{array}
    \]
  \end{minipage}

  \vfill
  \textbf{Remarks:}
  \begin{itemize}
  \item Distinct minimum and optimizer.
  \item Problems are nevertheless equivalent.
  \end{itemize}
  
\end{frame}

\section{Maximum Likelihood Estimation}

\begin{frame}{Gaussian Location Estimation}

  Suppose we observe data samples $X_i \in \Re^d$ for $i\in[1, \dots, N]$ \emph{drawn independently} from an \emph{Gaussian distribution} with unknown mean $\mu_0 \in C$ in a convex set $C$ and given variance $\Sigma\in S^d_{++}$. 

  \vfill

  The maximum likelihood estimator (\textbf{MLE}) is the solution to
  \[
    \max_{\mu \in C} ~\ell(X_1, \dots, X_N; \mu) \defn \prod_{i=1}^N\tfrac{\exp{\left(-\tfrac {(X_i-\mu)\tpose \Sigma^{-1}(X_i-\mu)}{2} \right)}}{\sqrt{(2\pi)^d \det(\Sigma)}}
  \]
  which is \textit{nonconvex} optimization problem.

  \vfill
  
  \uncover<2->{
  \textbf{Log-transformation:}
  \[
    \max_{\mu \in C} ~\log(\ell(X; \mu))=\textstyle -\frac 12 \left(\sum_{i=1}^N  (X_i-\mu)\tpose\Sigma^{-1}(X_i-\mu) + N \log(\det(\Sigma)) + N d \log(2\pi) \right).
  \]
  is an equivalent convex quadratic optimization problem.
  }
\end{frame}

\begin{frame}{Transformations}


  Suppose we have the transformations
  \begin{itemize}
  \item $\phi_0$ and $\phi_i$ for $i\in[1, \dots, m]$ \emph{monotone increasing} functions,
  \item $\psi_i$ for $i\in[1,\dots, \ell]$ are \emph{invertible}.
  \end{itemize}

  \vfill

  The following problems are \textbf{equivalent}
  
  \begin{minipage}{0.5\textwidth}
    \[
      \begin{array}{rl}
        \minimize & f(x) \\[0.5em]
        \st & g_i(x)\leq 0,\quad \forall i\\[0.5em]
                  & h_i(x) = 0, \quad \forall i.
      \end{array}
    \]
  \end{minipage}%
  \vline
  \begin{minipage}{0.5\textwidth}
    \[
      \begin{array}{rl}
        \minimize & \phi_0(f(x)) \\[0.5em]
        \st & \phi_i(g_i(x))\leq \phi_i(0),\quad \forall i\\[0.5em]
                  & \psi_i(h_i(x)) = \psi_i(0), \quad \forall i .
      \end{array}
    \]
  \end{minipage}


\end{frame}


\section{Support Vector Machines}

\begin{frame}{Linear Classification}

  \begin{itemize}

  \item \emph{Training set} $\{(x_i, y_i)\}_{i=1}^N$ : covariates $x_i\in\Re^p$ and class labels $y_i \in \{-1, 1\}$.

  \item \emph{Classifier} $h(x) = sign(x\tpose w + w_0)$

  \item \emph{Strict Separation}
    \[
      \left\{
        \begin{array}{rl}
          x_i\tpose w + w_0 > 0, & \rm{if~} y_i=+1,\\
          x_i\tpose w +  w_0 < 0, & \rm{if~} y_i=-1
        \end{array}
      \right.
      \iff
      y_i[x_i\tpose w + w_0] > 0, \, \forall i\in[1, \dots, N]
    \]

    \vfill

    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[
          ticks=none,
          y=1.5cm,            % y unit vector
          x=2.5cm,
          % hide y axis,        % hide the y axis
          axis lines = none,
          x label style={at={(axis description cs:0.5,0.1)},anchor=north},
          y label style={at={(axis description cs:0.25,0.5)},anchor=south},
          clip=false
          ]

          \addplot[draw=black, line width = 0.5pt, domain=-1:1.1, samples=200]{0.8-0.2*x};

          \node[draw=red, fill=red, circle, inner sep=0.5] at (axis cs:1, 1) {};
          \node[draw=red, fill=red, circle, inner sep=0.5] at (axis cs:0.8, 1.3) {};
          \node[draw=red, fill=red, circle, inner sep=0.5] at (axis cs:-0.2, 1.4) {};
          \node[draw=red, fill=red, circle, inner sep=0.5] at (axis cs:-0.6, 1.6) {};
          \node[draw=red, fill=red, circle, inner sep=0.5] at (axis cs:0, 1.2) {};

          \node[draw=blue, fill=blue, circle, inner sep=0.5] at (axis cs:0.7, 0.2) {};
          \node[draw=blue, fill=blue, circle, inner sep=0.5] at (axis cs:0.5, 0.1) {};
          \node[draw=blue, fill=blue, circle, inner sep=0.5] at (axis cs:-0.15, 0.2) {};
          \node[draw=blue, fill=blue, circle, inner sep=0.5] at (axis cs:-0.45, 0.34) {};
          \node[draw=blue, fill=blue, circle, inner sep=0.5] at (axis cs:0.1, 0.3) {};

          \node (L) at (axis cs:1.1, 0.6) {};
          \node[right] at (L) {\tiny $L=\{x : x\tpose w + w_0=0 \}$};
          

        \end{axis}
      \end{tikzpicture}
    \end{center}

    \vfill

  \item<2-> \textbf{Remark:} When $w^\star, w_0^\star$ is feasible, then so is $\lambda w^\star, \lambda w_0^\star$ for any $\lambda>0$.
    
  \end{itemize}

\end{frame}

\begin{frame}{Classification Margin}

  \begin{itemize}
  \item The margin between point $(x_i, y_i)$ and the classifier $L$ is
    \[
      \left\{
        \begin{array}{rl}
          (x_i\tpose w + w_0)/\norm{w}_2, & \rm{if~} y_i=+1,\\
          -(x_i\tpose w + w_0)/\norm{w}_2, & \rm{if~} y_i=-1
        \end{array}\right.
      \iff
      y_i(x_i\tpose w + w_0)/\norm{w}_2
    \]
    
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[
          ticks=none,
          y=1.5cm,            % y unit vector
          x=2.5cm,
          % hide y axis,        % hide the y axis
          axis lines = none,
          x label style={at={(axis description cs:0.5,0.1)},anchor=north},
          y label style={at={(axis description cs:0.25,0.5)},anchor=south},
          clip=false
          ]

          \addplot[draw=black, line width = 0.5pt, domain=-1:1.1, samples=200]{0.8-0.2*x};

          \node[draw=red, fill=red, circle, inner sep=0.5] at (axis cs:1, 1) {};
          \node[draw=red, fill=red, circle, inner sep=0.5](x) at (axis cs:0.8, 1.3) {};
          \node[draw=red, fill=red, circle, inner sep=0.5] at (axis cs:-0.2, 1.4) {};
          \node[draw=red, fill=red, circle, inner sep=0.5] at (axis cs:-0.6, 1.6) {};
          \node[draw=red, fill=red, circle, inner sep=0.5] at (axis cs:0, 1.2) {};

          \node[draw=blue, fill=blue, circle, inner sep=0.5] at (axis cs:0.7, 0.2) {};
          \node[draw=blue, fill=blue, circle, inner sep=0.5] at (axis cs:0.5, 0.1) {};
          \node[draw=blue, fill=blue, circle, inner sep=0.5] at (axis cs:-0.15, 0.2) {};
          \node[draw=blue, fill=blue, circle, inner sep=0.5] at (axis cs:-0.45, 0.34) {};
          \node[draw=blue, fill=blue, circle, inner sep=0.5] at (axis cs:0.1, 0.3) {};

          \node (L) at (axis cs:1.1, 0.6) {};
          \node[right] at (L) {\tiny $L=\{x : x\tpose w + w_0=0 \}$};
          \node[draw=black, fill=black, circle, inner sep=0.5] (x_0) at (axis cs:0.765, 0.8-0.2*0.765) {};
          \draw[<->, densely dotted] (x_0) -- (x);
          

        \end{axis}
      \end{tikzpicture}
    \end{center}
    \vspace{1em}
  \item<2-> The \emph{maximum margin classifier}
    \[
      \max_{w\in\Re^p,~w_0\in\Re} \, \min_{i\in[1,\dots, N]}~ y_i(x_i\tpose w + w_0)/\norm{w}_2
    \]
    results in a \textit{nonconvex} optimization problem.
  \end{itemize}
\end{frame}

\begin{frame}{Classification Margin}

  An \emph{epigraphical transformation}
  \[
    \begin{array}{rl}
      \maximize & M \\[0.5em]
      \st & M \in \Re_+, ~w\in\Re^p,~w_0\in\Re\\[0.5em] 
                & y_i(x_i\tpose w + w_0)/\norm{w}_2 \geq M, \quad i \in [1, \dots, n]
    \end{array}
  \]

  \vfill

  \uncover<2->{
  An monotone increasing \emph{transformation}
  \[
    \begin{array}{rl}
      \maximize & M \\[0.5em]
      \st & M \in \Re_+, ~w\in\Re^p,~w_0\in\Re\\[0.5em] 
                & y_i(x_i\tpose w + w_0) \geq M \norm{w}_2, \quad i \in [1, \dots, n]
    \end{array}
  \]
  }
  
  \vfill
  \begin{itemize}
    \item<3-> \textbf{Recall:} When $w^\star, w_0^\star$ is feasible, then so is $\lambda w^\star, \lambda w_0^\star$ for any $\lambda>0$. Enforce arbitrary constraint $1/\norm{w}_2=M$.
    
  \end{itemize}

\end{frame}

\begin{frame}{Reformulation as an quadratic optimization problem}

  \textbf{Equivalent formulation : }
  \[
    \begin{array}{rl}
      \maximize & \tfrac{1}{\norm{w}_2} \\[0.5em]
      \st & w\in\Re^p,~w_0\in\Re\\[0.5em] 
                & y_i(x_i\tpose w + w_0) \geq 1, \quad i \in [1, \dots, n]
    \end{array}
  \]

  \vfill
  
  \textbf{Maximum margin classifier : }
  \[
    \begin{array}{rl}
      \minimize & \norm{w}_2^2 \\[0.5em]
      \st & w\in\Re^p,~w_0\in\Re\\[0.5em] 
                & y_i(x_i\tpose w + w_0) \geq 1, \quad i \in [1, \dots, n]
    \end{array}
  \]
  is a convex quadratic optimization problem.
  
\end{frame}

\section{Lyapunov Stability}

\begin{frame}{Dynamical System}

  \begin{minipage}{0.67\textwidth}
    Let $A \in \Re^{n\times n}$ and $x_0\in \Re^n$. Consider the \emph{dynamical system}:
    \[
      \begin{array}{rl}
        \frac{\d x(t)}{\d t} & = A x(t) \\[0.5em]
        x(0) & =x_0
      \end{array}
    \]
    
  \end{minipage}%
  \hfill
  \begin{minipage}{0.33\textwidth}
    \begin{center}
      \includegraphics[height=3cm]{lyapunov.jpg}\\
      Lyapunov
    \end{center}
  \end{minipage}

  \vfill
  The origin is an \emph{equilibrium} point, i.e.\ $x_0=0$ implies $x(t)=0$ for all $t$. \\
  A dynamical system is called \emph{stable} if $x(t)\to 0$ as $t\to\infty$ for any $x_0$.

  \vfill
  \textbf{Applications : } Electrical circuits, mechanical designs, \dots
  
\end{frame}

\begin{frame}{Lyapunov's idea}

  Find a \emph{Lyapunov function} $V:\Re^n\to\Re_+$ (differentiable) such that
  \begin{itemize}
  \item $V(x_0) = 0 \iff x_0=0$
  \item $\norm{x_0}\to \infty \implies V(x_0) \to \infty$
  \item $\dot V(x) = \iprod{V'(x)}{\frac{\d x}{\d t}} < 0$ for any $x$
  \end{itemize}

  \vfill

  \begin{minipage}{0.5\textwidth}
    The \emph{chain rule} of differentiation indicates that 
    \[
      \frac{\d V(x(t))}{\d t} = \dot V(x(t))  < 0
    \]
    along any trajectory $x(t)\neq 0$.
  \end{minipage}%
  \hfill
  \begin{minipage}{0.5\textwidth}
    \begin{center}
      \includegraphics[height=3.5cm]{lyapunov-function.jpg}
    \end{center}
  \end{minipage}

  \vfill

  \textbf{Theorem :} A Lyapunov function $V$ exists $\iff$ the origin is stable. 

\end{frame}

\begin{frame}{Stability}

  Find a \emph{Lyapunov function} $V:\Re^n\to\Re_+$ such that
  \begin{itemize}
  \item $V(x) = 0 \iff x=0$
  \item $\norm{x_0}\to \infty \implies V(x_0) \to \infty$
  \item $\dot V = \iprod{V'(x)}{\frac{\d x}{\d t}} < 0$ for any $x\neq 0$
  \end{itemize}

  \vfill

  Consider \emph{quadratic} Lyapunov functions : $V(x) = \iprod{P x}{x}$ with $P\in S^n$.
  \begin{itemize}
  \item $P\in S^n_{++}$
  \item $\dot V = \iprod{\d x/\d t}{P x} + \iprod{x}{P \d x/\d t} = \iprod{A x}{P x} + \iprod{x}{P A x} = \iprod{x}{(A\tpose P + P A)x}$
  \item<2-> (Parametrization is without loss of generality of linear systems)
  \end{itemize}

  \vfill

  \uncover<3->{
  \textbf{Feasibility problem :} When there exists $P\in S^n_{++}$ such that $-(A\tpose P + P A) \in S^n_{++}$ then system is stable. FYI this is equivalent to $\Re\{{\rm{eigs}}(A)\}<0$.}
\end{frame}

\begin{frame}{Decay Rate}

  \emph{Lyapunov exponent} of a trajectory of a dynamical system is defined as
  \[
    \alpha^\star \defn \sup\set{\alpha\geq 0}{\lim_{t\to\infty} ~\exp(\alpha t)\norm{x(t)}=0, ~~\forall x(0)\neq 0}
  \]
  and quantifies the exponential speed with which $x(t)$ decays to the origin.

  \vfill

  \textbf{Fact:} If $\dot{V}(x)\leq -2\alpha V(x)$ then $\alpha\leq \alpha^\star$ for linear systems.
  \[
    \dot{V}(x)\leq -2\alpha V(x) \iff \iprod{\d x/\d t}{P x} + \iprod{x}{P \d x/\d t} = \iprod{A x}{P x} + \iprod{x}{P A x} \leq -2\alpha \iprod{x}{Px}
  \]

  \vfill
  
  \uncover<2->{
  \emph{Decay Rate:}
  \[
    \begin{array}{rl}
      \alpha^\star\geq \maximize & \alpha\\[0.5em]
            \st & P\in S^n_{++},~\alpha\geq 0\\[0.5em]
                & (A\tpose P + P A) \preceq_{S^n_{+}}- 2\alpha P
    \end{array}
  \]
  }
\end{frame}

\section{Optimal Control}

\begin{frame}{Optimal Control}

  Consider the optimal control problem  
  \[
    \begin{array}{rl}
      \min_{x(t), u(t)} & J(x, u) \defn \displaystyle \int_0^\infty x(t)\tpose Q x(t) + u(t)\tpose R u(t)\\[0.5em]
                  \st      & \frac{\d x(t)}{\d t} = A x(t) + B u(t), \\[0.5em]
                        & x(0)=x_0, ~\lim_{T\to\infty} x(T)=0
    \end{array}
  \]
  with $Q\in \S^n_{++}$ and $R\in \S^m_{+}$.

  \vfill

  \textbf{Remarks:}
  \begin{itemize}
  \item It is difficult to optimize over the functions $x$ and $u$ directly
  \item<2-> Let us parameterize $u(t) = K x(t)$ and optimize over $K\in \Re^{m\times n}$ instead
  \item<3-> (Parametrization is without loss of optimality for linear systems) 
  \end{itemize}
  
\end{frame}

\begin{frame}{Linear Control}

  \textbf{Remarks:}
  \begin{itemize}
  \item The closed loop system becomes
    \[
      x(t) = A x(t) + B u(t) = (A + B K) x(t)
    \]
  \item<2-> Cost is $J(x, Kx) = \int_0^\infty x(t)\tpose (Q+K\tpose R K) x(t) = x_0\tpose V^{-1} x_0$ where $V\in S^n_+$ solves the algebraic Riccati equation (ARE)
    \[
      (A+BK) V + V (A+BK) \tpose -B R^{-1} B\tpose + V Q V = 0
    \]
  \item<3-> Stability constraint is redundant
    \begin{align*}
       x_0\tpose V^{-1} x_0 < \infty \implies & \lim_{T\to\infty} x(T)=0
    \end{align*}
  \end{itemize}
  
\end{frame}

\begin{frame}{Nonconvex Formulation}

  Optimization formulation
  \[
    \begin{array}{rl}
      \min_{K,  V} & x_0\tpose  V^{-1} x_0 \\[0.5em]
      \st      &  (A+BK) V + V (A+BK) \tpose -B R^{-1} B\tpose + V Q V = 0 \\[0.5em]                     
               & V \succeq_{\S^n_+} 0, ~K\in \Re^{m\times n}.
    \end{array}
  \]

  \vfill

  \textbf{Remarks:}
  \begin{itemize}
  \item The objective $x_0\tpose V^{-1} x_0$ is convex in $ V$
  \item The ARE constraint is quadratic in $V$ and bilinear in $K V$
  \end{itemize}
  
\end{frame}


\begin{frame}{Algebraic Riccati Equations [$\star$]}

  \textbf{Two important observations concerning the ARE}
  
  \begin{enumerate}
  \item Monotonicity
    \[
      (A+BK) \bar V + \bar V (A+BK)\tpose -B R^{-1} B\tpose + \bar V Q \bar V \preceq_{\S^n_+} 0 \implies \bar V^{-1} \succeq _{\S^n_+} V^{-1}
    \]
  \item<2-> Semidefinite representation (S-lemma)
    \begin{align*}
      & (A+BK) \bar V + \bar V (A+BK)\tpose -B R^{-1} B\tpose + \bar V Q \bar V \preceq_{\S^n_+} 0\\[0.5em]
        \iff & \begin{pmatrix} B R^{-1} B\tpose - A\bar V-BK\bar V  -  \bar V A\tpose -\bar V K\tpose B\tpose & \bar V \\[0.5em] \bar V & Q^{-1} \end{pmatrix} \succeq_{\S^n_+} 0.
    \end{align*}
  \end{enumerate}
  
\end{frame}

\begin{frame}{Nonconvex Formulation}

    Optimization formulation
  \[
    \begin{array}{rl}
      \min_{K, \bar V} & x_0\tpose \bar V^{-1} x_0 \\[0.5em]
      \st      &  \begin{pmatrix} B R^{-1} B\tpose - A\bar V-BK\bar V  -  \bar V A\tpose -\bar V K\tpose B\tpose & \bar V \\[0.5em] \bar V & Q^{-1} \end{pmatrix} \succeq_{\S^n_+} 0 \\[1.5em]                    
               & \bar V\succeq_{\S^n_+} 0, ~K\in\Re^{m\times n}
    \end{array}
  \]

  \vfill

  \textbf{Remarks:}
  \begin{itemize}
  \item The objective $x_0\tpose \bar V^{-1} x_0$ is convex in $\bar V$
  \item The constraint is nonconvex as we still have bilinear products $K\bar V$
  \item<2-> Introduce new variable $Z=K\bar V \in \Re^{m\times n}$
  \item<2-> For every $Z$ there exists a $K=Z \bar V^{-1}$ such that $Z=K\bar V \in \Re^{m\times n}$
  \end{itemize}
  
\end{frame}

\begin{frame}{Nonconvex Formulation}

    Optimization formulation
  \[
    \begin{array}{rl}
      \min_{K, \bar V} & x_0\tpose \bar V^{-1} x_0 \\[0.5em]
      \st      &  \begin{pmatrix} B R^{-1} B\tpose - A\bar V-B Z  -  \bar V A\tpose -Z\tpose B\tpose & \bar V \\[0.5em] \bar V & Q^{-1} \end{pmatrix} \succeq_{\S^n_+} 0 \\[1.5em]                    
               & \bar V\succeq_{\S^n_+} 0, ~K\in\Re^{m\times n}
    \end{array}
  \]

  \vfill

  \textbf{Remarks:}
  \begin{itemize}
  \item Convex in $\bar V$ and $Z$!
  \item<2-> Optimal $K^\star = Z^\star (\bar V^{\star})^{-1}$!
  \end{itemize}
  
\end{frame}

\section{Probability Inequalities}

\begin{frame}{Digital Communication Limits}

	Consider a set of possible messages $\mc S=\{s_1, \dots, s_c\}\subseteq \mb R^2$. 
	The signals are communicated over a noisy channel perturbed by additive noise.
	
	\begin{minipage}{0.5\columnwidth}
	\centering
	
	 \begin{tikzpicture}[scale=1]
	 	\matrix[ampersand replacement=\&, row sep=0.2cm, column sep=0.6cm] {
	      		\node[rectangle, minimum width=3cm, minimum height=1cm, fill=gray!20, draw] (chan) {};
			\node[right] at ($(chan.north west)+(-1mm, 2mm)$) {channel};
			\node[circle, draw] (add) at ($(chan.east)+(-5mm, 0mm)$) {+};
			\&
			\node[rectangle, minimum width=1cm, minimum height=1cm, draw] (dec) {Dec};\\
		};
		\draw[->] ($(add.north)+(0mm, 5mm)$)--(add.north);
		\node[left] at ($(add.north)+(0mm, 5mm)$) {$\xi$};
		\draw[->] ($(add.west)+(-25mm, 0mm)$)--(add.west);
		\node[above] at ($(add.west)+(-25mm, 0mm)$) {$s_t$};
		\draw[->] (add.east)--(dec.west) {};
		\node[above] at ($0.5*(dec.west)+0.5*(chan.east)$) {$s_o$};
		\draw[->] (dec.east)--($(dec.east)+(4mm, 0mm)$) {};
		\node[above] at ($(dec.east)+(4mm, 0mm)$) {$s_r$};
	\end{tikzpicture}
	\end{minipage}%
	\hfill
	\begin{minipage}{0.5\columnwidth}
	\centering
	\includegraphics[height = 4cm]{constellation}
	\end{minipage}
	
	
	\textbf{Problem: } Bound the probability of correct transmission
	\[
	  p = 1- \mathbb P( s_i +\xi \notin C_i ),
	\]
	where $C_i=\set{x}{\norm{x-s_i}_2<\norm{x-s_j}_2~\forall j<i,~\norm{x-s_i}_2\leq\norm{x-s_j}_2~\forall j>i}$ is the set of outputs that are decoded as $s_i$. We know \emph{only} the mean $\mu = \E{}{\xi}$ and second moment $S = \E{}{\xi\cdot \xi\tpose}$ of the mean.
	 
\end{frame}


\begin{frame}{Probability Inequalities in Statistics}

  \textbf{Markov's Inequality :}
  Suppose $X\geq 0$ and $\mu=\E{}{X}$. Then, $$\mb P(X\geq k)\leq \mu/k.$$
  \textbf{Proof :} $\mb P(X\geq k)  = \E{}{1_{X\geq k}} \leq \E{}{X/k} = \mu/k$.

  \vfill

  \uncover<2->{
  \textbf{Chebyshev's Inequality:}
  Suppose $\mu=\E{}{X}$ and $\sigma^2=\E{}{(X-\mu)^2}$. Then, $$\mb P(\abs{X-\mu}\geq k \sigma)\leq 1/k^2.$$
  \textbf{Proof :} $\mb P(\abs{X-\mu}\geq k \sigma)  = \E{}{1_{\abs{X-\mu}\geq k \sigma}} \leq \E{}{{(X-\mu)^2}/({k\sigma})^2} = 1/k^2$.
  }
\end{frame}

\begin{frame}{Generalizing Markov and Chebyshev}

  Let $X\in\Re^d$ be a random vector.
  \begin{itemize}
  \item $\E{}{f_i(X)}=m_i \in E_i$ for some (moment) functions $f_1:\Re^d\to E_1, \dots, f_m:\Re^d\to E_{m}$.
  \item $\E{}{f_0(X)}=m_0=1$ for $f_0=1$.
  \item Best upper bound on $\mb P(X\in C)$ for $C\subseteq \Re^n$?
  \end{itemize}

  \vfill

  \uncover<2->{
    \textbf{Trick :} $\mb P(X \in C) = \E{}{1_C(X)}$, where $1_C(x)=1$ if $x\in C$; zero otherwise.
  }

  \vfill

  \uncover<3->{
  Assume that $f(x)=\sum_{i=0}^m \iprod{\lambda_i}{f_i(x)} \geq 1_C(x)$ for all $x$, then
  \[
    \sum_{i=0}^m \iprod{\lambda_i}{m_i} = \E{}{\sum_{i=0}^m \iprod{\lambda_i}{f_i(X)} }\geq \E{}{1_C(X)} = \mb P(X \in C).
  \]
  }
\end{frame}

\begin{frame}{Best Upper Bound}

  Best upper bound using optimization
  \[
    \mb P(X \in C) \leq \min_{\lambda_i\in E_i} \set{\sum_{i=0}^m \iprod{\lambda_i}{m_i}}{\sum_{i=0}^m \iprod{\lambda_i}{f_i(x)}\geq 1~\forall x \in C, ~~\sum_{i=0}^m \iprod{\lambda_i}{f_i(x)} \geq 0~~\forall x}
  \]
  is a \textbf{convex optimization} problem!

  \vfill

  \uncover<2->{
  \textbf{Remarks:}
  \begin{itemize}
  \item \emph{Duality theory} proves this bound to be \emph{information theoretically optimal}.
  \item<3-> \emph{Semi-infinite optimization}: Finite number of variables for an infinite number of constraints.
  \end{itemize}
  }

\end{frame}

\begin{frame}{Special case for mean and variance}

  \textbf{We know :} $\E{}{2X}=2\mu \in \Re^d$ and $\E{}{XX\tpose}=S\in \S^{d}$.\\
  \textbf{We want :} bounds on $\mb P(X\in C)$ for a given set $C \in \Re^n$.\\

  \vfill

  \uncover<2->{
    Here we have
    \[
      \textstyle f(x) \defn \iprod{\lambda_0}{1} + \iprod{\lambda_1}{2x} + \iprod{\lambda_2}{x x \tpose}
    \]
    for $\lambda_0\in\Re$, $\lambda_1\in \Re^d$ and $\lambda_2 \in S^d$. \gray{Recall $\iprod{A}{B}=\tr(A B )$ for elements in $S^d$.}
  }
  \vfill

  \uncover<3->{
    \emph{Best upper bound} on $\mb P(X \in C)$ using optimization
    \[
      \begin{array}{rl}
        \minimize_{} & \lambda_0 + 2\iprod{\lambda_1}{\mu} + \iprod{\lambda_2}{S} \\[0.5em]
        \st & \lambda_0\in \Re,~\lambda_1\in\Re^d,~\lambda_2\in S^d \\[0.5em]
                     &  \iprod{\lambda_0}{1} + \iprod{\lambda_1}{2x} + \iprod{\lambda_2}{x x \tpose} \geq 1\quad\forall x \in C\\[0.5em]
                     & \iprod{\lambda_0}{1} + \iprod{\lambda_1}{2x} + \iprod{\lambda_2}{x x \tpose} \geq 0\quad\forall x\in\Re^n.
      \end{array}
    \]
    is a \textbf{convex optimization} problem!
  }
\end{frame}


\section{Antenna Arrays}

\begin{frame}{Isotropic Harmonic Oscillator}

  The \emph{electromagnetic field} $E_0(t)$ at time $t$ from an \emph{isotropic harmonic oscillator} with wavelength $\lambda$ and frequency $\omega$ is
  \[
    E_0(t) \defn \Re\{ \tfrac{z_0}{\norm{P-P_0}}  \cdot \exp(i \cdot (\omega t - 2\pi \tfrac{\norm{P-P_0}}{\lambda})) \}
  \]
  where
  \begin{itemize}
  \item $P_0$ : Location parameter,
  \item $z_0\in \C$\, : Phase parameter.
  \end{itemize}

  \vfill

  \textbf{Diagram:}
  \begin{center}
    \includegraphics[height=3.5cm]{julia/iho.pdf}
  \end{center}
\end{frame}

\begin{frame}{Antenna Array}

  \emph{Antenna Array:} $n$ isotropic antennas at locations $P_k$.\\
  The total electromagnetic field is
  \begin{align*}
    E(t) & = \sum_{k=1}^n \Re\{ \tfrac{z_k}{\norm{P-P_k}}  \cdot \exp(i \cdot (\omega t - 2\pi \tfrac{\norm{P-P_k}}{\lambda})) \}\\[-.5em]
         & = \Re\{\exp(i \omega t)\cdot \sum_{k=1}^n  \tfrac{z_k}{\norm{P-P_k}}  \cdot \exp(- 2\pi i \tfrac{\norm{P-P_k}}{\lambda})) \}
  \end{align*}

  \vfill

  \uncover<2->{
  Let location $P=(r, \theta)$ be in polar coordinates with $r\gg 1$. Similarly $P_k=(r_k, \theta_k)$. We may ignore terms of the order $O(r^{-2})$. \emph{Far field} at $P$ is approximately

  \[
    E(t) = \Re\{\underbrace{\tfrac{1}{r} \cdot \exp(i \cdot (\omega t - 2\pi \tfrac{r}{\lambda})}_{A(r)}\cdot \underbrace{\sum_{k=1}^n  z_k  \cdot \exp(- 2\pi i r_k\tfrac{\cos(\abs{\theta-\theta_k})}{\lambda})}_{D(\theta)} \}.
  \]
  }
  
  \begin{itemize}
  \item<3-> Amplitude $A$ is a function of distance $r$.
  \item<4-> Direction $D$ is a function of the angle $\theta$.
  \end{itemize}
  
\end{frame}

\begin{frame}{Diagram of an Antenna Array}

  Suppose we want the antenna array to be \emph{sensitive to} $\phi\in R=[-\pi/6, \pi/6]$.

  \vfill
  
  \emph{Sidelobe angle energy} : $\max_{\phi \not \in R} \abs{D(\phi)}^2$
  
  \emph{Normalization}: $\Re\{D(0)\}\geq 1$
  
  \vfill

  Array with minimum sidelobe angle energy
  \begin{equation*}
    \begin{array}{rl}
      \min_{z_k\in \rm{C}, \, \tau\in \Re} & \tau \\[0.5em]
      \st & \tau \geq \abs{\sum_{k=1}^n  z_k  \cdot \exp(- 2\pi i r_k\tfrac{\cos(\abs{\theta-\theta_k})}{\lambda})} \quad \forall \theta\not\in R,\\[0.5em]
                                    & \Re\{\sum_{k=1}^n  z_k  \cdot \exp(- 2\pi i r_k\tfrac{\cos(\abs{\theta_k})}{\lambda})\}\geq 1.
    \end{array}
  \end{equation*}
  is a semi-infinite (complex!) convex optimization problem.

  \vfill


\end{frame}

\begin{frame}{Antenna Synthesis}

  \textbf{Parameters:}
  \begin{itemize}
  \item Array locations $P_k=(k, 0)$ with $k\in [1, 20]$.
  \item Wavelength $\lambda = 4$
  \item Angles of interest $R=[-\pi/6, \pi/6]$ (use gridding).
  \end{itemize}

  \vfill
  
  \textbf{Result:}

  \begin{center}
    \includegraphics[height=3.5cm]{julia/aa.pdf}
  \end{center}
  
\end{frame}

\section{Portfolio Optimization}

\begin{frame}{Portfolio Optimization}

  Portfolio with $n$ assets or stocks held over a period of time. Return of each asset $i$ is a random variable $R
  _i$ with mean $\bar r \defn \E{}{R} \in \Re^n$ and variance $\Sigma \defn \E{}{(R-\bar r)\cdot (R-\bar r)\tpose} \in \S^n_+$.

  \vfill

  Terms
  \begin{itemize}
  \item $x_i \in \Re_+$: Percentage we which to \emph{invest} in stock $i$, i.e.,
    \(
      \sum_{i=1}^p x_i = 1
    \)
  \item Expected \emph{Profit} : $\bar p = \E{}{P\defn \sum_{i=1}^n R_i \cdot x_i} = \sum_{i=1}^n \bar{r}_i \cdot x_i$
  \item \emph{Variance} : $\E{}{(P-\bar p)^2} = x\tpose \Sigma x$
  \end{itemize}

  \vfill

  \begin{minipage}{0.66\textwidth}
    Markowitz Portfolio Formulation:
    \[
      \begin{array}{rl}
        \minimize & x\tpose \Sigma x \\[0.5em]
        \st & x_i\in \Re_+,~\sum_{i=1}^p x_i = 1  \\[0.5em]
                  & \sum_{i=1}^n \bar{r}_i \cdot x_i \geq r_{\min} \\
      \end{array}
    \]
  \end{minipage}%
  \hfill
  \begin{minipage}{0.33\textwidth}
    \includegraphics[height=2.5cm]{markowitz.jpg}\\
    Markowitz
  \end{minipage}
  
  
\end{frame}

\begin{frame}{Portfolio Optimization -- Extension}

  Portfolio with $n$ assets or stocks held over a period of time. Return of each asset $i$ is a random variable $R
  _i$ with mean $\bar r \defn \E{}{R} \in \Re^n$ and variance $\Sigma \defn \E{}{(R-\bar r)\cdot (R-\bar r)\tpose} \in \S^n_+$.

  \vfill

  Terms
  \begin{itemize}
  \item $x_{\rm{long}}\in \Re^n_+$, $x_{\rm{short}}\in \Re^n_+$: Percentage we which to invest \emph{long/short} in stock $i$
  \item Expected Profit : $\bar p = \E{}{p\defn R \tpose x_{\rm{long}} - R \tpose x_{\rm{short}}} = \bar{r}\tpose (x_{\rm{long}} -x_{\rm{short}}) $
  \item Variance : $\E{}{(p-\bar p)^2} =   (x_{\rm{long}} -x_{\rm{short}}) \tpose \Sigma (x_{\rm{long}} -x_{\rm{short}})  $
  \end{itemize}

  \vfill

  Markowitz Portfolio Formulation:
  \[
    \begin{array}{rl}
      \minimize &  (x_{\rm{long}} -x_{\rm{long}}) \tpose \Sigma (x_{\rm{long}} -x_{\rm{long}})  \\[0.5em]
      \st & x_{\rm{long}} \in \Re^n_+, ~x_{\rm{short}} \in \Re^n_+,~ 1\tpose x_{\rm{long}} + 1\tpose x_{\rm{short}} = 1  \\[0.5em]
                & \bar{r}\tpose (x_{\rm{long}} -x_{\rm{short}}) \geq r_{\min} \\
    \end{array}
  \]
  
\end{frame}

\begin{frame}{Financial crisis of 2007--2008}

  \begin{minipage}{0.3\textwidth}
    Nassim Taleb:\\
    \includegraphics[height=3.5cm]{taleb.jpg}
  \end{minipage}%
  \hfill
  \begin{minipage}{0.7\textwidth}

    {``That’s why portfolio theory simply doesn’t work. It uses metrics like variance to describe risk, while most real risk comes from a single observation, so variance is a volatility that doesn’t really describe the risk. \textbf{It’s very foolish to use variance}.''}
    
  \end{minipage}
  

\end{frame}


\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End: