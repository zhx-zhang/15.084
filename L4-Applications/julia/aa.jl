using DataFrames
using CSV

using Convex
using ECOS

#######
## Antenna array parameters
#######

# Sidelobe angles
S = π/6:0.01:(2*π-π/6)
# Wavelenght
λ = 4
# Position Pₖ = (k, 0). Thus θₖ=0.
n = 20
r = 1:n

##########
## Optimization variables
##########
z = ComplexVariable(n)
τ = Variable(1)

##########
## Optimization constraints
##########
c_1 = [real(sum([z[k]*exp(-2*π*im*r[k]*cos(0)/λ) for k in 1:n]))>=1, τ>=0]
c_2 = [τ >= abs(sum([z[k]*exp(-2*π*im*r[k]*cos(θ)/λ) for k in 1:n])) for θ in S]
c = c_1+c_2

problem = minimize(τ, c)
solve!(problem, ECOSSolver(maxit=1000))

###
## Save results for plotting
###

A = 0:0.01:2*π
D = [ abs(sum([z.value[k]*exp(-2*π*im*r[k]*cos(θ)/λ) for k in 1:n])) for θ in A]
R = DataFrame(theta=A*360/(2*π), D = D/maximum(D))
CSV.write("aa.csv", R)

