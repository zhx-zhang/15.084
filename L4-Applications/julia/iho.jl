using DataFrames
using CSV

theta = range(0,stop=360,length=200)
D = ones(200)

R = DataFrame(theta=theta, D=D)
CSV.write("iho.csv", R)
